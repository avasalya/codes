
# sudo chmod +x rename_files.bash




###############################
### to save in same folder
###############################

# jsoncounter=1
# pngcounter=1


echo -n "Enter starting number: "
read count
pngcounter=$count
jsoncounter=$count

echo -n "files will be renamed starting from:"
echo $jsoncounter, $pngcounter


mkdir -p dataset

for file in `\find . -maxdepth 1 -mindepth 1 -type f | sort`; do

    if [[ $file = *.png ]]; then
        echo $file
        cp $file ./dataset/"$pngcounter".png
        pngcounter=`expr $pngcounter + 1`
    elif [[ $file = *.json ]]; then
        echo $file
        cp $file ./dataset/"$jsoncounter".json
        jsoncounter=`expr $jsoncounter + 1`
    fi
done











###############################
### to save in separate folders
###############################



# jsoncounter=99
# pngcounter=99

# mkdir -p renamed_png
# mkdir -p renamed_json

# for file in `\find . -maxdepth 1 -mindepth 1 -type f | sort`; do

#     if [[ $file = *.png ]]; then
#         echo $file
#         cp $file ./renamed_png/"$pngcounter".png
#         pngcounter=`expr $pngcounter + 1`
#     elif [[ $file = *.json ]]; then
#         echo $file
#         cp $file ./renamed_json/"$jsoncounter".json
#         jsoncounter=`expr $jsoncounter + 1`
#     fi

# done