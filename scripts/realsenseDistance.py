# -*- coding: utf-8 -*-

############################################# 
## D435 Depth image Display 
# source http://mirai-tec.hatenablog.com/entry/2018/07/29/150902
#############################################  
import pyrealsense2 as rs
import numpy as np
import cv2

# Stream (Depth/Color) settings
config = rs.config()
config.enable_stream(rs.stream.color, 640 , 480 , rs. format .bgr8, 30 )
config.enable_stream(rs.stream.depth, 640 , 480 , rs. format .z16, 30 )

# Start streaming
pipeline = rs.pipeline()
profile = pipeline.start(config)

# Distance [m] = depth * depth_scale 
depth_sensor = profile.get_device().first_depth_sensor()
depth_scale = depth_sensor.get_depth_scale()
clipping_distance_in_meters = 1.0  # meter
clipping_distance = clipping_distance_in_meters / depth_scale

# Align object generation
align_to = rs.stream.color
align = rs.align(align_to)

try :
     while  True :
        # Wait for frame (Color & Depth)
        frames = pipeline.wait_for_frames()
        aligned_frames = align.process(frames)
        color_frame = aligned_frames.get_color_frame()
        depth_frame = aligned_frames.get_depth_frame()
        if  not depth_frame or  not color_frame:
            continue

        color_image = np.asanyarray(color_frame.get_data())
        depth_image = np.asanyarray(depth_frame.get_data())
        # Depth image pre-processing (image within 1m)   
        gray_color = 153
        depth_image_3d = np.dstack((depth_image,depth_image,depth_image))
        bg_removed = np.where((depth_image_3d >clipping_distance) | (depth_image_3d <= 0 ), gray_color, color_image)
        
        # Rendering 
        depth_colormap = cv2.applyColorMap(cv2.convertScaleAbs(depth_image, alpha= 0.03 ), cv2.COLORMAP_JET)
        images = np.hstack((bg_removed, depth_colormap))
        # cv2.namedWindow( 'Align Example' , cv2.WINDOW_AUTOSIZE)
        cv2.imshow( 'RealSense' ,images)
        # cv2.imshow( 'RealSense' , color_image)
        if cv2.waitKey( 1 ) & 0xff == 27 :
            break

finally :
     # Stop streaming
    pipeline.stop()
    cv2.destroyAllWindows()