""" cross check annoatated dataset for yolo """

import os
import sys
import time
import getpass
import cv2 as cv
import numpy as np

username = getpass.getuser()
osName = os.name
if osName == 'posix':
    os.system('clear')
else:
    os.system('cls')

def draw_box(x, y, w, h, img):
    
    dh = img.shape[0]
    dw = img.shape[1]
    w_ = int(w*dw)
    h_ = int(h*dh)
    x_ = int(x*dw)
    y_ = int(y*dh)

    # remember yolo (x,y) is at center of the box 
    x_ = int(x_- w_/2)
    y_ = int(y_- h_/2)
    w_ = x_ + w_
    h_ = y_ + h_

    print("image space yolo_bbox (x, y, w, h)", x_, y_, w_, h_)
    img = cv.rectangle(img, (x_, y_), (w_, h_), (255, 0, 255), 2)

    cv.imshow('yolo bbox', img)


if __name__ == "__main__":
    
    startTime = time.time()
    
    # path to dir
    # path = os.path.dirname(__file__) + '/../images/onigiriTx/'
    path = os.path.dirname(__file__) + '/../onigiriTx/'
    #  path = os.path.dirname(__file__) + '/../images/onigiriyolo4dataset/txOnigiriStack/'
    
    print(path)

    # MAIN loop
    Samples = 49

    for i in range(Samples):
        
        fileName = str(i+1)

        # paths
        RGBfile = path + "images/" + fileName + ".png"
        labels  = path + "labels/" + fileName + ".txt"
        
        img = cv.imread(RGBfile)
        
        # read one line at a time and plot yolo box
        file = open(labels, 'r')
        for lines in file:
            # visualize bbox draw_box(x, y, w, h, img)
            x = float(lines.split()[1])
            y = float(lines.split()[2])
            w = float(lines.split()[3])
            h = float(lines.split()[4])
            draw_box(x, y, w, h, img)
        
        # comment this if you don't want to visualize yolo bbox
        cv.waitKey(50)
        
    finishedTime = time.time()
    print('\nfinished in', round(finishedTime-startTime, 2), 'second(s)')
