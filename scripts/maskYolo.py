""" stream using realsense and detect onigiri using yolo"""
# -*- coding: utf-8 -*-

import time
import argparse
import cv2 as cv
import numpy as np
import random as rng
import pyrealsense2 as rs


# # onigiri network parameters
parser = argparse.ArgumentParser(add_help=False)
parser.add_argument("--weights", default='yolov3/yolov3-onigiri.backup', help="YOLO weights path")
parser.add_argument("--config", default='yolov3/yolov3-onigiri.cfg', help="YOLO config path")
parser.add_argument("--names", default='yolov3/onigiri.names', help="class names path")
args = parser.parse_args()


""" https://www.pyimagesearch.com/2015/02/16/faster-non-maximum-suppression-python/ """

def nms(box, confThresh=None, overlapThresh=0.3):

    # if there are no boxes, return an empty list
    if len(box) == 0:
        print("no box found")
        return []

    # # if the bounding boxes integers, convert them to floats --
    # # this is important since we'll be doing a bunch of divisions
    boxes = np.array(box)

    # initialize the list of picked indexes	
    pick = []

    # grab the coordinates of the bounding boxes
    x1 = boxes[:,0]
    y1 = boxes[:,1]
    x2 = boxes[:,2]
    y2 = boxes[:,3]

    # compute the area of the bounding boxes and sort the bounding
    # boxes by the bottom-right y-coordinate of the bounding box
    area = (x2 - x1 + 1) * (y2 - y1 + 1)
    idxs = y2
    
    # if confidence probabilities are provided, sort on them instead
    if confThresh is not None:
        idxs = confThresh

    # sort the indexes
    idxs = np.argsort(idxs)
    
    # keep looping while some indexes still remain in the indexes list
    while len(idxs) > 0:

        # grab the last index in the indexes list and add the
        # index value to the list of picked indexes
        last = len(idxs) - 1
        i = idxs[last]
        pick.append(i)

        # find the largest (x, y) coordinates for the start of
        # the bounding box and the smallest (x, y) coordinates
        # for the end of the bounding box
        xx1 = np.maximum(x1[i], x1[idxs[:last]])
        yy1 = np.maximum(y1[i], y1[idxs[:last]])
        xx2 = np.minimum(x2[i], x2[idxs[:last]])
        yy2 = np.minimum(y2[i], y2[idxs[:last]])

        # compute the width and height of the bounding box
        w = np.maximum(0, xx2 - xx1 + 1)
        h = np.maximum(0, yy2 - yy1 + 1)

        # compute the ratio of overlap
        overlap = (w * h) / area[idxs[:last]]
                
        # delete all indexes from the index list that have
        idxs = np.delete(idxs, np.concatenate(([last], np.where(overlap > overlapThresh)[0])))
        # return only the bounding boxes that were picked using the
        # integer data type
    return boxes[pick].astype("int")




if __name__ == '__main__':

    CONF_THRESH, NMS_THRESH = 0.75, 0.3

    # Load the network
    net = cv.dnn.readNetFromDarknet(args.config, args.weights)
    net.setPreferableBackend(cv.dnn.DNN_BACKEND_CUDA) # DNN_BACKEND_OPENCV
    net.setPreferableTarget(cv.dnn.DNN_TARGET_CUDA) # DNN_TARGET_CPU

    # Stream (Color/Depth) settings
    config = rs.config()
    config.enable_stream(rs.stream.color, 640 , 480 , rs.format.bgr8, 60)
    config.enable_stream(rs.stream.depth, 640 , 480 , rs.format.z16, 60)
    config.enable_stream(rs.stream.infrared, 640 , 480 , rs.format.y8, 60)

    # Start streaming
    pipeline = rs.pipeline()
    profile = pipeline.start(config)

    try:
        while  True:
            # Wait for frame (Color & Depth)
            frames = pipeline.wait_for_frames()
            color_frame = frames.get_color_frame()
            depth_frame = frames.get_depth_frame()
            
            if  not depth_frame or  not color_frame:
                continue
            color_image = np.asanyarray(color_frame.get_data())

            # Depth image
            depth_color_frame = rs.colorizer().colorize(depth_frame)
            depth = np.asanyarray(depth_color_frame.get_data())
            
            # Read and convert the image to blob and perform forward pass 
            # to get the bounding boxes with their confidence scores
            rgb = color_image
            height, width = rgb.shape[:2]

    
            # Get the output layer from YOLO
            layers = net.getLayerNames()
            output_layers = [layers[i[0] - 1] for i in net.getUnconnectedOutLayers()]

            blob = cv.dnn.blobFromImage(rgb, 0.00392, (416, 416), swapRB=True, crop=False)
            net.setInput(blob)
            start = time.time()
            layer_outputs = net.forward(output_layers)
            end = time.time()

            # show timing information on YOLO
            print("[INFO] YOLO took {:.6f} seconds".format(end - start))

            class_ids, confidences, boxes = [], [], []

            for output in layer_outputs:

                for detection in output:

                    scores = detection[5:]
                    class_id = np.argmax(scores)
                    confidence = scores[class_id]

                    if confidence > CONF_THRESH:

                        center_x, center_y, w, h = (detection[0:4] * np.array([width, height, width, height])).astype('int')

                        x = int(center_x - w / 2)
                        y = int(center_y - h / 2)

                        boxes.append([x, y, int(w), int(h)])
                        confidences.append(float(confidence))
                        class_ids.append(int(class_id))

            # print("confidence", confidences)
            # print("total boxes", len(boxes))

            # # Draw the filtered bounding boxes with their class to the image
            with open(args.names, "r") as f:
                classes = [line.strip() for line in f.readlines()]
            # colors = np.random.uniform(0, 255, size=(len(classes), 3))
            color = (255,0,255)

            # Perform non maximum suppression for the bounding boxes to filter overlapping and low confident bbox
            pick = nms(boxes, confidences, NMS_THRESH)
            # print("after applying non-maximum, total bounding boxes", len(boxes), "reduced to", (len(pick)))

            # draw bounding boxes
            i = 0
            for (x, y, w, h) in pick:
                    cv.rectangle(rgb, (x, y), (x + w, y + h), color, 2)
                    cv.rectangle(depth, (x, y), (x + w, y + h), color, 2)
                    if i < len(pick):
                        cv.putText(rgb, classes[class_ids[i]], (x, y-5), cv.FONT_HERSHEY_COMPLEX_SMALL, 1, color, 1)
                        cv.putText(depth, classes[class_ids[i]], (x, y-5), cv.FONT_HERSHEY_COMPLEX_SMALL, 1, color, 1)
                        i+=1

            
            
            """ *************************************************************************** """
            # remove noise from depth
            ret, thresh = cv.threshold(rgb, 130, 255, cv.THRESH_BINARY_INV)
            morph = cv.morphologyEx(thresh, cv.MORPH_DILATE, cv.getStructuringElement(cv.MORPH_ELLIPSE, (1,1))); #a,c
                        
            # region of interest
            roi = cv.bitwise_and(rgb.copy(), morph.copy())
            roi = cv.cvtColor(roi, cv.COLOR_BGR2GRAY)
            # cv.imshow("roi", roi)
            
            """ look for largest contour(s) """
            def desiredContour(c): 
                peri = cv.contourArea(c) # Find areas
                good =  peri > 500 and peri < 10000
                return  good # Large area is considered "good"

            """ Find and Draw contours """
            morphd_8u = roi.astype('uint8')
            contours, hierarchy = cv.findContours(morphd_8u, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_NONE)
            print('total contours', len(contours))

            drawing = np.zeros((morphd_8u.shape[0], morphd_8u.shape[1], 3))
            for c in contours:
                if desiredContour(c):
                    cv.drawContours(drawing, [c], -1,  (0,255,0),  1, cv.LINE_4, hierarchy, 0)

            # draw largest contours
            c = max(contours, key = cv.contourArea)
            x,y,w,h = cv.boundingRect(c)
            # draw the biggest contour (c) in green
            rectimg = cv.rectangle(drawing,(x,y),(x+w,y+h),(255,0,0),2)
            rectroi = cv.rectangle(roi,(x,y),(x+w,y+h),(255,0,0),2)
            # mask = cv.bitwise_and(rectroi, rectimg)
            
            """ create back mask """
            mask = np.zeros(roi.shape, dtype="uint8")
            mask[y:y+h, x:x+w] = 255
            # cv.imshow("mask", mask)


            """ extract and create roi mask """
            # roimask = cv.bitwise_and(mask, onigirid.copy()) #depth image
            roimask = cv.bitwise_and(mask, roi.copy()) #gray image
            cv.imshow('roi mask', roimask)



            """ find edges around onigiri """
            edges = cv.Canny(roimask,100,255) 
            
            # cv.imshow('roi edges', edges)
            # cv.imshow("rectroi", rectroi)
            # images = np.hstack((rgb, rectimg))
            
            """ *************************************************************************** """
            rgb[np.all(rgb==255, axis=2)] = 0
            kernel = np.array([[1, 1, 1], [1, -8, 1], [1, 1, 1]], dtype=np.float32)
                        
                                
            # imgLaplacian = cv.filter2D(rgb, -1, kernel)
            imgLaplacian = cv.filter2D(rgb, cv.CV_32F, kernel)

            sharp = np.float32(rgb)
            imgResult = sharp - imgLaplacian
            # cv.imshow('New Sharped Image', imgLaplacian)

            # convert back to 8bits gray scale
            imgResult = np.clip(imgResult, 0, 255)
            imgResult = imgResult.astype('uint8')
            imgLaplacian = np.clip(imgLaplacian, 0, 255)
            imgLaplacian = np.uint8(imgLaplacian)
            # cv.imshow('New Sharped Image', imgResult)


            # Create binary image from source image
            # bw = cv.threshold(cv.cvtColor(imgResult, cv.COLOR_BGR2GRAY), abs(mean-std), 255, cv.THRESH_BINARY_INV | cv.THRESH_OTSU)[1]
            bw = cv.threshold(edges, 100, 255, cv.THRESH_BINARY | cv.THRESH_OTSU)[1]
            # rect, bw = cv.threshold(roimask, 10, 255, cv.THRESH_BINARY_INV | cv.THRESH_OTSU)

            # cv.imshow('Binary Image', bw)


            # kernel = np.ones((3,3),np.uint8)
            # morph = cv.morphologyEx(bw, cv.MORPH_OPEN, kernel, iterations = 2)
            # morph = cv.morphologyEx(bw, cv.MORPH_DILATE, cv.getStructuringElement(cv.MORPH_ELLIPSE, (1,1))); #a,c

            # Perform the distance transform algorithm
            dist = cv.distanceTransform(bw, cv.DIST_L2, 3)
            # dist = cv.distanceTransform(morph, cv.DIST_L2, 5)

            # Normalize the distance image for range = {0.0, 1.0}
            # so we can visualize and threshold it
            cv.normalize(dist, dist, 0.05, 1.0, cv.NORM_MINMAX)
            # cv.imshow('Distance Transform Image', dist)


            # Threshold to obtain the peaks # This will be the markers for the foreground objects
            _, dist = cv.threshold(dist, 0.1, 1.0, cv.THRESH_BINARY)
            # _, dist = cv.threshold(dist, 0.1, 1.0, cv.THRESH_TOZERO)


            # Dilate a bit the dist image
            # kernel = np.ones((3,3),np.float32)/9 
            # dist = cv.dilate(dist, kernel)
            # cv.imshow('Peaks', dist)


            imgStack = np.hstack((roimask, edges, bw, dist))
            # cv.imshow('roimask, edges, bw, dist', imgStack)



            # Create the CV_8U version of the distance image
            # It is needed for findContours()
            dist_8u = dist.astype('uint8')

            # Find total markers
            contours, hierarchy = cv.findContours(dist_8u, cv.RETR_CCOMP, cv.CHAIN_APPROX_SIMPLE)
            print('total contours', len(contours))

            # Create the marker image for the watershed algorithm
            markers = np.zeros(dist.shape, dtype=np.int32)
            # markers = np.zeros(dist.shape, dtype=np.int8)


            # Draw the foreground markers
            for i in range(len(contours)):
                        cv.drawContours(markers, contours, i, (i+1))


            # Draw the background marker
            # cv.circle(markers, (5,5), 3, (255,255,255), -1)
            # cv.imshow('Markers', markers.astype('uint8'))

            # Perform the watershed algorithm
            cv.watershed(imgResult, markers)
            # cv.watershed(imgResult, markers.astype('int32'))

            #mark = np.zeros(markers.shape, dtype=np.uint8)
            mark = markers.astype('uint8')
            # mark = cv.bitwise_and(mark, roimask)
            mark = cv.bitwise_and(mark, edges)
            # cv.imshow('Markers_v2', mark)

            out = mark
            # out = markers

            # Generate random colors
            colors = []
            for contour in contours:
                colors.append((rng.randint(0,256), rng.randint(0,256), rng.randint(0,256)))

            # Create the result image
            dst = np.zeros((out.shape[0], out.shape[1], 3), dtype=np.uint8)

            # Fill labeled objects with random colors
            for i in range(out.shape[0]):
                for j in range(out.shape[1]):
                    index = out[i,j]
                    if index > 0 and index <= len(contours):
                        dst[i,j,:] = colors[index-1]


            # Visualize the final image
            images = np.hstack((imgResult, dst))
            # cv.imshow('sharp, colorsegmentation', imgStack2)

            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            """ *************************************************************************** """

            # images = np.hstack((rgb, depth))
            cv.namedWindow( 'RealSense' ,cv.WINDOW_AUTOSIZE)
            cv.imshow( 'RealSense' ,images)
            
            if cv.waitKey( 1 ) & 0xff == 27 :
                break

    finally:
        # Stop streaming
        print("stopping streaming...")
        pipeline.stop()
        cv.destroyAllWindows()

