""" histogram backprojection """

from __future__ import print_function
from __future__ import division

import os
import sys
import cv2 as cv
import numpy as np
import argparse
import getpass
import matplotlib.pyplot as plt 


username = getpass.getuser()
osName = os.name
if osName == 'posix':
    os.system('clear')
else:
    os.system('cls')


path = os.path.dirname(__file__) + "/../images/onigiriGrid/"


""" calculate depth image surface normals """

def surfnorm(img):
    """Estimate surface normal from depth image.

    Code is adapted from an OpenCV answer:
    http://answers.opencv.org/question/82453
    (Answer1, Solution1)

    :param img:
    :type img: numpy.ndarray, shape=(height, width)
    """
    h, w = img.shape


    dzdx = (img[2:, 1:-1] - img[0:-2, 1:-1]) / 2.0
    dzdy = (img[1:-1, 2:] - img[1:-1, 0:-2]) / 2.0
    nmat = np.stack([-dzdx, -dzdy, np.ones((h - 2, w - 2))], axis=2)
    norm = np.linalg.norm(nmat, axis=2)
    nmat = nmat / norm[:, :, np.newaxis]
    pad0 = np.zeros((nmat.shape[0], 1, 3))
    nmat = np.hstack([pad0, nmat, pad0])
    pad1 = np.zeros((1, nmat.shape[1], 3))
    nmat = np.vstack([pad1, nmat, pad1])

    return nmat[:, :, 0], nmat[:, :, 1], nmat[:, :, 2]


img = cv.imread(path + 'a/11/depth.png', cv.CV_8UC1)
print(img.shape)
print(img.dtype)

# test for surfnorm
nx, ny, nz = surfnorm(img)
print(nx.shape)
print(ny.shape)
print(nz.shape)

from mpl_toolkits.mplot3d import Axes3D
# fig = plt.figure()
# fig.suptitle("surface normal")
# ax1 = Axes3D(fig)
# ax1.scatter(np.ravel(nx), np.ravel(ny), np.ravel(nz), s=1, depthshade=False)
plt.figure()
plt.suptitle("surface normal about xyz")
plt.subplot(131)
plt.imshow(nx, cmap='gray')
plt.subplot(132)
plt.imshow(ny, cmap='gray')
plt.subplot(133)
plt.imshow(nz, cmap='gray')

plt.show()