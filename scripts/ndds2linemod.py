""" convert pose from ndds JSON to Linemod YML format """

import os
import sys
import time
import json
import yaml
import shutil
import getpass
import cv2 as cv
import numpy as np
import random as rand
from scipy.spatial.transform import Rotation as R

username = getpass.getuser()
osName = os.name
if osName == 'posix':
    os.system('clear')
else:
    os.system('cls')


def draw_box(x, y, w, h, img):
    
    x = int(x)
    y = int(y)
    w = x + w
    h = y + h
    img = cv.rectangle(img, (x, y), (w, h), (255*rand.uniform(0,1), 55*rand.uniform(0,1), 125*rand.uniform(0,1)), 2)
    cv.imshow('ndds bbox', img)

def ndds2linemod(img, jsonfile, gtyml, i):

    dummy = []

    with open(str(jsonfile)) as attributes:
        data = json.load(attributes)

    # extract bounding box
    for obj in range(len(data["objects"])):
        
        # https://github.com/j96w/DenseFusion/issues/76#issuecomment-632612484
        bbox = data['objects'][obj]['bounding_box']

        # ignore when visibility is 0
        visibility = data['objects'][obj]['visibility']
        if visibility is 0:
            print("file, objId, visibility", i, obj, visibility)
            continue

        # remember ndds x,y are reversed
        bbox_x = round(bbox['top_left'][1])
        bbox_y = round(bbox['top_left'][0])
        bbox_w = round(bbox['bottom_right'][1]) - bbox_x
        bbox_h = round(bbox['bottom_right'][0]) - bbox_y
        
        xywh = [bbox_x, bbox_y, bbox_w, bbox_h]
        
        # visualize ndds bbox
        draw_box(bbox_x, bbox_y, bbox_w, bbox_h, img)

        # NDDS gives units in centimeters
        translation = np.array(data['objects'][obj]['location']) * 10

        quaternion_obj2cam = R.from_quat(np.array(data['objects'][obj]['quaternion_xyzw']))
        quaternion_cam2world = R.from_quat(np.array(data['camera_data']['quaternion_xyzw_worldframe']))
        quaternion_obj2world = quaternion_obj2cam * quaternion_cam2world

        rotation = np.dot(quaternion_obj2world.as_dcm(), np.array([[-1, 0, 0], [0, -1, 0], [0, 0, 1]]))

        # print("rotation", rotation)
        # print("rotation", rotation.flatten().tolist())
        # print("translation", translation)
        # print("translation", translation.flatten().tolist())
        # print("bbox cor", xywh)


        dummy.append({
                    'cam_R_m2c':rotation.flatten().tolist(),
                    'cam_t_m2c':translation.flatten().tolist(), 
                    'obj_bb': xywh, 
                    'obj_id': 1 #obj
                })


    # write to yml format
    out = {i:dummy}
    with open(gtyml, 'a') as f:
        yaml.dump(out, f, default_flow_style=None)


def setLabeling(trainTest, lowerRange, upperRange):

    open(trainTest, 'w').close()
    with open(trainTest, "a") as f:
        for i in range(lowerRange, upperRange):
             #ignore initial 2 zeros
            fileName = str(names[randomList[i]])[2:]
            print(fileName)
            f.write(fileName + "\n")
    print("saved", trainTest)



if __name__ == "__main__":
    
    startTime = time.time()
    
    # path to dir
    thisfilepath = os.path.dirname(__file__)
    srcpath = '/media/ash/SSD/Odaiba/Telexistance/dataset/synthetic/modernHome_640/'
    dstpath = '/home/ash/DenseFusion/datasets/txonigiri/data/01/'

    # inputs 
    names = []
    Samples = 50
    copyfiles = True

    # ground truth yml file
    gtyml = dstpath + "gt.yml"

    # erase old yml file
    open(gtyml, 'w').close()

    # main
    for i in range(Samples):
        
        if i<10:
            fileName = "00000" + str(i)
        elif i>9 and i<100:
            fileName = "0000" + str(i)
        elif i>99 and i<1000:
            fileName = "000" + str(i)
        else:
            fileName = "00" + str(i)

        names.append(fileName)

        # read RGB-D, mask, json files
        jsonfile  = srcpath + fileName + ".json"
        rgbfile   = fileName + ".png"
        depthfile = fileName + ".depth.cm.8.png"
        maskfile  = fileName + ".cs.png"

        if copyfiles:
            shutil.copyfile((srcpath + rgbfile), (dstpath + "rgb/" + rgbfile))
            shutil.copyfile((srcpath + depthfile), (dstpath + "depth/" + depthfile))
            shutil.copyfile((srcpath + maskfile), (dstpath + "mask/" + maskfile))
        
        # read rgbfile image
        image = cv.imread(srcpath + rgbfile)

        # convert to linemode format
        ndds2linemod(image, jsonfile, gtyml, i)
        
        # comment this if you don't want to visualize bbox
        if i is -1:
            cv.waitKey(5000)
        elif i is -110:
            cv.waitKey(5000)
        else:
            cv.waitKey(100)


    """ create test/train sets """
    
    # unique random number b/w 1 to N(sample size)
    randomList = rand.sample(range(0, Samples), Samples)
    print("randomlist", randomList)
    print("creating train and test sets as per random order 90%-10%")

    # write xx% train files location in random order
    traintxt = dstpath + "train.txt"
    setLabeling(traintxt, 0, int(Samples*0.90))

    # write 100-xx% test files location in random order
    testtxt = dstpath + "test.txt"
    setLabeling(testtxt, int(Samples*0.90), Samples)


    finishedTime = time.time()
    print('\nfinished in', round(finishedTime-startTime, 2), 'second(s)')