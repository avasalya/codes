""" need python3.x """
############################################# 
## D435 Depth image Display 
# source http://mirai-tec.hatenablog.com/entry/2018/07/29/150902
############################################# 
import matplotlib.pyplot as plt
import pyrealsense2 as rs
import numpy as np
import cv2 as cv

# Stream (Color/Depth) settings
config = rs.config()
config.enable_stream(rs.stream.color, 640 , 480 , rs. format .bgr8, 30 )
config.enable_stream(rs.stream.depth, 640 , 480 , rs. format .z16, 30 )
config.enable_stream(rs.stream.infrared, 640 , 480 , rs. format .y8, 30 )

# Start streaming
pipeline = rs.pipeline()
profile = pipeline.start(config)

try :
     while  True :
        # Wait for frame (Color & Depth)
        frames = pipeline.wait_for_frames()
        color_frame = frames.get_color_frame()
        depth_frame = frames.get_depth_frame()

        if  not depth_frame or  not color_frame:
            continue
        color_image = np.asanyarray(color_frame.get_data())
        
        # Depth image
        depth_color_frame = rs.colorizer().colorize(depth_frame)
        depth_color_image = np.asanyarray(depth_color_frame.get_data())

        # Display
        images = np.hstack((color_image, depth_color_image))
        cv.namedWindow( 'RealSense' ,cv.WINDOW_AUTOSIZE)
        # cv.imshow( 'RealSense' ,images)
        cv.imshow( 'RealSense' , color_image)
        if cv.waitKey( 1 ) & 0xff == 27 :
            break

        # plt.imshow(color_image)
        # plt.show()

finally :
     # Stop streaming
    pipeline.stop()
    cv.destroyAllWindows()