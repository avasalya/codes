""" convert pose from ndds JSON to Linemod YML format """

import os
import sys
import time
import json
import yaml
import shutil
import getpass
import cv2 as cv
import numpy as np
import random as rand
from scipy.spatial.transform import Rotation as R

username = getpass.getuser()
osName = os.name
if osName == 'posix':
    os.system('clear')
else:
    os.system('cls')


def draw_box(x, y, w, h, img):
    
    x = int(x)
    y = int(y)
    w = x + w
    h = y + h
    img = cv.rectangle(img, (x, y), (w, h), (255*rand.uniform(0,1), 55*rand.uniform(0,1), 125*rand.uniform(0,1)), 2)
    cv.imshow('ndds bbox', img)


def ndds2linemod(img, jsonfile, gtyml, i):

    dummy = []

    with open(str(jsonfile)) as attributes:
        data = json.load(attributes)

    # extract bounding box
    for obj in range(len(data["objects"])):
        
        # https://github.com/j96w/DenseFusion/issues/76#issuecomment-632612484
        bbox = data['objects'][obj]['bounding_box']

        # # fully occluded USE ONLY FOR MULTI-OBJ IMAGES
        # visibility = data['objects'][obj]['visibility']
        # if visibility is 0: # ignore when visibility is 0
        #     print("file, objId, visibility", i, obj, visibility)
        #     continue

        # remember ndds x,y are reversed
        bbox_x = round(bbox['top_left'][1])
        bbox_y = round(bbox['top_left'][0])
        bbox_w = round(bbox['bottom_right'][1]) - bbox_x
        bbox_h = round(bbox['bottom_right'][0]) - bbox_y
        
        xywh = [bbox_x, bbox_y, bbox_w, bbox_h]
        
        # visualize ndds bbox
        draw_box(bbox_x, bbox_y, bbox_w, bbox_h, img)

        # NDDS gives units in centimeters -- convert to meters
        translation = np.array(data['objects'][obj]['location']) * 10

        # y-axis mirror rotation transform
        quaternion_obj2cam = R.from_quat(np.array(data['objects'][obj]['quaternion_xyzw']))
        quaternion_cam2world = R.from_quat(np.array(data['camera_data']['quaternion_xyzw_worldframe']))
        quaternion_obj2world = quaternion_obj2cam * quaternion_cam2world
        rotation = np.dot(quaternion_obj2world.as_dcm(), np.array([[-1, 0, 0], [0, -1, 0], [0, 0, 1]]))

        # print("rotation", rotation)
        # print("rotation", rotation.flatten().tolist())
        # print("translation", translation)
        # print("translation", translation.flatten().tolist())
        # print("bbox cor", xywh)

        dummy.append({
                    'cam_R_m2c':rotation.flatten().tolist(),
                    'cam_t_m2c':translation.flatten().tolist(), 
                    'obj_bb': xywh, 
                    'obj_id': 1 #obj
                })

    # write to yml format
    out = {i:dummy}
    with open(gtyml, 'a') as f:
        yaml.dump(out, f, default_flow_style=None)


def setLabeling(trainTest, lowerRange, upperRange):

    with open(trainTest, "a") as f:
        for i in range(lowerRange, upperRange):
            fileName = str(names[randomList[i]])
            # print(fileName)
            f.write(fileName + "\n")
    print("saved", trainTest)



if __name__ == "__main__":
    
    startTime = time.time()
    
    # inputs 
    names = []
    sample = []
    n = 0

    # path to source, all folders must have same file structure
    srcpath = [

            '/media/ash/SSD/Odaiba/Telexistance/dataset/synthetic/Home_living_640/',
            '/media/ash/SSD/Odaiba/Telexistance/dataset/synthetic/Home_kitchen_640/',
            '/media/ash/SSD/Odaiba/Telexistance/dataset/synthetic/Home_bedroom_640/',
            '/media/ash/SSD/Odaiba/Telexistance/dataset/synthetic/Home_balcony_640/',

            ]
    
    # path to destination
    dstpath = '/home/ash/DenseFusion/datasets/txonigiri/data/01/'

    shutil.rmtree(dstpath + "rgb/")
    shutil.rmtree(dstpath + "depth/")
    shutil.rmtree(dstpath + "mask/")
    shutil.rmtree(dstpath + "json/")
    
    os.mkdir(dstpath + "rgb/")
    os.mkdir(dstpath + "depth/")
    os.mkdir(dstpath + "mask/")
    os.mkdir(dstpath + "json/")

    # gt, train, test files
    gtyml = dstpath + "gt.yml"
    traintxt = dstpath + "train.txt"
    testtxt = dstpath + "test.txt"
    
    # erase old files
    open(gtyml, 'w').close()
    open(traintxt, 'w').close()
    open(testtxt, 'w').close()

    # count total no of frames (all folders)
    for fi in range(len(srcpath)):
        count = len([name for name in os.listdir(srcpath[fi]) if os.path.isfile(os.path.join(srcpath[fi], name))])
        sample.append(int((count-2)/4)) # /4 because there are 4 types of files in each src folder

        for i in range(sample[fi]):
            
            # read ndds files
            if i<1:
                infileName = "00000"
            elif i>0 and i<10:
                infileName = "00000"
            elif i>9 and i<100:
                infileName = "0000"
            elif i>99 and i<1000:
                infileName = "000"
            else:
                infileName = "00"

            infileName = infileName + str(i)
            
            # export linemod files
            if n<1:
                outfileName = "000"
            elif n>0 and n<10:
                outfileName = "000"
            elif n>9 and n<100:
                outfileName = "00"
            elif n>99 and n<1000:
                outfileName = "0"
            else:
                outfileName = ""

            names.append(outfileName + str(n))

            # read RGB-D, mask, json files
            rgbfile   = infileName + ".png"
            depthfile = infileName + ".depth.cm.8.png"
            maskfile  = infileName + ".cs.png"
            jsonfile  = infileName + ".json"

            # copy files to respective folders
            shutil.copyfile((srcpath[fi] + rgbfile), (dstpath + "rgb/" + outfileName + str(n) + ".png"))
            shutil.copyfile((srcpath[fi] + depthfile), (dstpath + "depth/" + outfileName + str(n) + ".png"))
            shutil.copyfile((srcpath[fi] + maskfile), (dstpath + "mask/" + outfileName + str(n) + ".png"))
            shutil.copyfile((srcpath[fi] + jsonfile), (dstpath + "json/" + outfileName + str(n) + ".json"))
            
            # read rgbfile image
            image = cv.imread(srcpath[fi] + rgbfile)

            # convert to linemode format
            ndds2linemod(image, (srcpath[fi]+jsonfile), gtyml, n)
            
            # comment this if you don't want to visualize bbox
            if i is -1:
                cv.waitKey(5000)
            elif i is -110:
                cv.waitKey(5000)
            else:
                cv.waitKey(100)

            n+=1



    """ create test/train sets """
    print("total samples are", n, "in packets of", sample)

    # unique random number b/w 1 to N(sample size)
    print("files name", names)
    randomList = rand.sample(range(0, n), n)
    print("random sets for training/testing", randomList)
    print("creating train and test sets as per random order 80-20")


    # write xx% train files location in random order
    setLabeling(traintxt, 0, int(n*0.80))

    # write 100-xx% test files location in random order
    setLabeling(testtxt, int(n*0.80), n)


    finishedTime = time.time()
    print('\nfinished in', round(finishedTime-startTime, 2), 'second(s)')