# -*- coding: UTF-8 -*-
import os
import cv2
import getpass
import numpy as np
import open3d.open3d as o3d
import matplotlib.pyplot as plt
# from openni import openni2

""" https://www.learnopencv.com/how-to-select-a-bounding-box-roi-in-opencv-cpp-python/ """

username = getpass.getuser()
print('using user-->', username)
osName = os.name

username = getpass.getuser()
osName = os.name
if osName == 'posix':
    os.system('clear')
else:
    os.system('cls')


path = os.path.dirname(__file__) + "/../images/onigiriGrid/"



""" custom ROI extractor
https://stackoverflow.com/questions/56658331/attributeerror-module-cv2-has-no-attribute-selectroi 
https://stackoom.com/question/3pjQ3/AttributeError-%E6%A8%A1%E5%9D%97-cv-%E6%B2%A1%E6%9C%89%E5%B1%9E%E6%80%A7-selectROI
"""
    
class ExtractImageWidget(object):
    
    def __init__(self):
        # self.original_image = cv2.imread('1.jpg')
        self.original_image = cv2.imread(path + "/depth1.png")

        # Resize image, remove if you want raw image size
        self.original_image = cv2.resize(self.original_image, (640, 556))
        self.clone = self.original_image.copy()

        cv2.namedWindow('image')
        cv2.setMouseCallback('image', self.extract_coordinates)

        # Bounding box reference points and boolean if we are extracting coordinates
        self.image_coordinates = []
        self.extract = False

    def extract_coordinates(self, event, x, y, flags, parameters):
        # Record starting (x,y) coordinates on left mouse button click
        if event == cv2.EVENT_LBUTTONDOWN:
            self.image_coordinates = [(x,y)]
            self.extract = True

        # Record ending (x,y) coordintes on left mouse bottom release
        elif event == cv2.EVENT_LBUTTONUP:
            self.image_coordinates.append((x,y))
            self.extract = False
            print('top left: {}, bottom right: {}'.format(self.image_coordinates[0], self.image_coordinates[1]))

            # Draw rectangle around ROI
            cv2.rectangle(self.clone, self.image_coordinates[0], self.image_coordinates[1], (0,255,0), 2)
            cv2.imshow("image", self.clone) 

        # Clear drawing boxes on right mouse button click
        elif event == cv2.EVENT_RBUTTONDOWN:
            self.clone = self.original_image.copy()
            
    def show_image(self):
        return self.clone






if __name__ == '__main__' :

    print('cv2 version', cv2.__version__)

    """ custom ROI extractor -- better for multiple boxes""" 
    # extract_image_widget = ExtractImageWidget()
    # while True:
    #     cv2.imshow('image', extract_image_widget.show_image())
    #     key = cv2.waitKey(1)

    #     # Close program with keyboard 'q'
    #     if key == ord('q'):
    #         cv2.destroyAllWindows()
    #         exit(1)




    """ ROI extractor article """
    # Read image
    im = cv2.imread(path + 'b/11/image.png')
    # im = cv2.rotate(im, cv2.ROTATE_90_CLOCKWISE)
    
    ## Select single ROI
    r = cv2.selectROI("select_a_ROI", im, fromCenter=True, showCrosshair=True)
    x,y,w,h = r
    mask = np.zeros_like(im, np.uint8)
    cv2.rectangle(mask, (x,y), (x+w, y+h), (255,255,255), -1)
    masked = cv2.bitwise_and(im, mask )
    cv2.imshow("mask", mask)
    cv2.imshow("ROI", masked) ## cropped image
    cv2.waitKey(0)



    # ## multiple ROIs
    # rect = []
    # rect = cv2.selectROIs("select_n_ROIs", im, showCrosshair=True, fromCenter=True)
    # # cv2.waitKey(5000)
    # print(rect)
    


    
    

    # # Crop image -- no need of this
    # imCrop = im[int(r[1]):i nt(r[1]+r[3]), int(r[0]):int(r[0]+r[2])]
    # # Display cropped image
    # cv2.imshow("cropped part", imCrop)
    
    
    
    """ try this """
    """ Capturing mouse click events with Python and OpenCV """
    """ https://www.pyimagesearch.com/2015/03/09/capturing-mouse-click-events-with-python-and-opencv/ """