""" move all ndds files into their specfic folders """

import os
import time
import shutil
import getpass


username = getpass.getuser()
osName = os.name
if osName == 'posix':
    os.system('clear')
else:
    os.system('cls')


if __name__ == "__main__":
    
    startTime = time.time()
    
    # path to dir
    path = os.path.dirname(__file__)
    path = path + '/../../../Unreal Projects/Dataset_Synthesizer/Source/NVCapturedData/banjuOnigiri2/'

    # create directory save annotated data (images + txt)
    folderNames = ["images", "cs", "is", "depth", "depth16", "depthcm8", "depthcm16", "depthmm16", "json", "labels"]
    for i in folderNames:
        if os.path.exists(path + i):
            pass
        else:
            os.mkdir(path + i);

    Samples = 1000

    """ use only when required """ 
    movefiles = True

    for i in range(Samples):
        
        if i<10:
            fileName = "00000" + str(i)
        elif i>9 and i<100:
            fileName = "0000" + str(i)
        elif i>99 and i<1000:
            fileName = "000" + str(i)
        else:
            fileName = "00" + str(i)

        # """ separate files into respective folders and move color images to same folder as bbox"""
        if movefiles is True:
            
            shutil.move((path + fileName + ".json"), path + "json")
            shutil.move((path + fileName + ".png"), path + "images")
            shutil.move((path + fileName + ".cs" +".png"), path + "cs")
            shutil.move((path + fileName + ".is" +".png"), path + "is")
            shutil.move((path + fileName + ".depth" +".png"), path + "depth")
            shutil.move((path + fileName + ".depth.16" +".png"), path + "depth16")
            shutil.move((path + fileName + ".depth.cm.8" +".png"), path + "depthcm8")
            shutil.move((path + fileName + ".depth.cm.16" +".png"), path + "depthcm16")
            shutil.move((path + fileName + ".depth.mm.16" +".png"), path + "depthmm16")
    
  
    finishedTime = time.time()
    print('\nfinished in', round(finishedTime-startTime, 2), 'second(s)')