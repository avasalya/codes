""" creating training(99%) and testing(1%) dataset for yolo """

import os
import time
import shutil
import getpass
import random as rand

username = getpass.getuser()
osName = os.name
if osName == 'posix':
    os.system('clear')
else:
    os.system('cls')
   

def write2File(trainTest, lowerRange, upperRange, copy=False):

    open(path + "../" + trainTest + ".txt", 'w').close()
    with open(path + "../" + trainTest + ".txt", "a") as f:
        
        for i in range(lowerRange, upperRange):
            
            fileName = str(randomList[i])
            f.write("dataset/images/" + fileName + ".png" + "\n")
            # move test images and labels to test folder
            if copy:
                shutil.move((path + fileName + ".png"), (path + "../" + "test"))
                shutil.move((path + "../labels/" + fileName + ".txt"), (path + "../" + "test"))




if __name__ == "__main__":

    startTime = time.time()

    Samples = 762

    # unique random number b/w 1 to N(sample size)
    randomList = rand.sample(range(1, Samples + 1), Samples)

    # path to dir
    path = os.path.dirname(__file__)
    path = path + '/../images/onigiriyolo4dataset/dataset/images/'
    
    
    # read file name
    # for file in os.listdir(path):
        # print(file)

    # # write 80% train files location in random order
    write2File("train", 0, int(Samples*0.99), False)

    # # write 20% test files location in random order
    write2File("test", int(Samples*0.99), Samples, True)

    finishedTime = time.time()
    print('\nfinished in', round(finishedTime-startTime, 5), 'second(s)')