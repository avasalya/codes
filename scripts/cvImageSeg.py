import os
import sys
import pickle
import getpass
import cv2 as cv
import numpy as np
import open3d.open3d as o3d
import matplotlib.pyplot as plt
import random as rng
import open3d.open3d as o3d

username = getpass.getuser()
osName = os.name
if osName == 'posix':
    os.system('clear')
else:
    os.system('cls')

# path to dir
path = os.path.dirname(__file__) + "/../images/onigiriGrid/w-wo-banju/"


# Image Segmentation Techniques
""" https://docs.opencv.org/3.4/d2/dbd/tutorial_distance_transform.html """

""" https://www.analyticsvidhya.com/blog/2019/04/introduction-image-segmentation-techniques-python/ """



"""************************************************** read maps **************************************************"""

onigiriFolder = 'a/2/'

""" read depth.png """
# banjud = cv.imread(path + 'a/0/depth1.png', cv.CV_16UC1)
# onigirid = cv.imread(path + onigiriFolder + 'depth1.png', cv.CV_16UC1)


""" read depth.pkl """
banjud = pickle.load(open(path + 'a/0/depth.pkl', 'rb'), encoding='bytes')
onigirid = pickle.load(open(path + onigiriFolder + 'depth.pkl', 'rb'), encoding='bytes')


""" read color.png """
onigiriC = cv.imread(path + onigiriFolder + 'image.png')
onigiriG = cv.imread(path + onigiriFolder + 'image.png', 0)



""" convert into uint8 """
banjud = np.uint8(banjud) # need better way to convert
onigirid = np.uint8(onigirid)


""" make copy of maps """
# banjud_copy = banjud.copy()
# onigirid_copy = onigirid.copy()



"""************************************************** depth map **************************************************"""


""" taking abs difference in pixel depth (fg-bg) """
depthDiff = cv.subtract(banjud, onigirid)


""" mean/std of depth fg-bg """
mean = np.nanmean(depthDiff[depthDiff > 0]) # alternative mean/median
std = np.std(depthDiff[depthDiff > 0])
print('mean', mean)
print('std', std)


""" remove noise """
ret, threshd = cv.threshold(depthDiff, abs(mean-std) , 255, cv.THRESH_BINARY)
morphd = cv.morphologyEx(threshd, cv.MORPH_DILATE, cv.getStructuringElement(cv.MORPH_ELLIPSE, (1,1))); #a,c


""" region of interest """
roi = cv.bitwise_and(onigiriG.copy(), morphd.copy())
# cv.imshow('roi', roi)


""" look for largest contour(s) """
def desiredContour(c): 
    peri = cv.contourArea(c) # Find areas
    good =  peri > 500
    return  good # Large area is considered "good"


""" Find and Draw contours """
morphd_8u = roi.astype('uint8')
contours, hierarchy = cv.findContours(morphd_8u, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_NONE)
print('total contours', len(contours))

drawing = np.zeros((morphd_8u.shape[0], morphd_8u.shape[1], 3))
for c in contours:
    if desiredContour(c):
        cv.drawContours(drawing, [c], -1,  (0,255,0),  1, cv.LINE_4, hierarchy, 0)

# draw largest contours
c = max(contours, key = cv.contourArea)
x,y,w,h = cv.boundingRect(c)
# draw the biggest contour (c) in green
rectimg = cv.rectangle(drawing,(x,y),(x+w,y+h),(255,0,0),2)
rectroi = cv.rectangle(roi,(x,y),(x+w,y+h),(255,0,0),2)
cv.imshow('contour', rectimg)


""" create back mask """
mask = np.zeros(roi.shape, dtype="uint8")
mask[y:y+h, x:x+w] = 255
# cv.imshow("mask", mask)


""" extract and create roi mask """
# roimask = cv.bitwise_and(mask, onigirid.copy()) #depth image
roimask = cv.bitwise_and(mask, roi.copy()) #gray image
cv.imshow('roi mask', roimask)



""" find edges around onigiri """
edges = cv.Canny(roimask,100,255) 
# cv.imshow('roi edges', edges)





"""************************************************** RGB map **************************************************"""

# # Change the background from white to black, since that will help later to extract
# # better results during the use of Distance Transform
onigiriC[np.all(onigiriC == 255, axis=2)] = 0



# Create a kernel that we will use to sharpen our image
# an approximation of second derivative, a quite strong kernel
kernel = np.array([[1, 1, 1], [1, -8, 1], [1, 1, 1]], dtype=np.float32)
# kernel = np.ones((3,3),np.float32)/9 

# do the laplacian filtering as it is
# well, we need to convert everything in something more deeper then CV_8U
# because the kernel has some negative values,
# and we can expect in general to have a Laplacian image with negative values
# BUT a 8bits unsigned int (the one we are working with) can contain values from 0 to 255
# so the possible negative number will be truncated

# imgLaplacian = cv.filter2D(onigiriC, -1, kernel)
imgLaplacian = cv.filter2D(onigiriC, cv.CV_32F, kernel)

sharp = np.float32(onigiriC)
imgResult = sharp - imgLaplacian
# cv.imshow('New Sharped Image', imgLaplacian)

# convert back to 8bits gray scale
imgResult = np.clip(imgResult, 0, 255)
imgResult = imgResult.astype('uint8')
imgLaplacian = np.clip(imgLaplacian, 0, 255)
imgLaplacian = np.uint8(imgLaplacian)
# cv.imshow('New Sharped Image', imgResult)


# Create binary image from source image
# bw = cv.threshold(cv.cvtColor(imgResult, cv.COLOR_BGR2GRAY), abs(mean-std), 255, cv.THRESH_BINARY_INV | cv.THRESH_OTSU)[1]
bw = cv.threshold(edges, 100, 255, cv.THRESH_BINARY | cv.THRESH_OTSU)[1]
# rect, bw = cv.threshold(roimask, 10, 255, cv.THRESH_BINARY_INV | cv.THRESH_OTSU)

# cv.imshow('Binary Image', bw)


# kernel = np.ones((3,3),np.uint8)
# morph = cv.morphologyEx(bw, cv.MORPH_OPEN, kernel, iterations = 2)
# morph = cv.morphologyEx(bw, cv.MORPH_DILATE, cv.getStructuringElement(cv.MORPH_ELLIPSE, (1,1))); #a,c

# Perform the distance transform algorithm
dist = cv.distanceTransform(bw, cv.DIST_L2, 3)
# dist = cv.distanceTransform(morph, cv.DIST_L2, 5)

# Normalize the distance image for range = {0.0, 1.0}
# so we can visualize and threshold it
cv.normalize(dist, dist, 0.05, 1.0, cv.NORM_MINMAX)
# cv.imshow('Distance Transform Image', dist)


# Threshold to obtain the peaks # This will be the markers for the foreground objects
_, dist = cv.threshold(dist, 0.1, 1.0, cv.THRESH_BINARY)
# _, dist = cv.threshold(dist, 0.1, 1.0, cv.THRESH_TOZERO)


# Dilate a bit the dist image
# kernel = np.ones((3,3),np.float32)/9 
# dist = cv.dilate(dist, kernel)
# cv.imshow('Peaks', dist)


imgStack = np.hstack((roimask, edges, bw, dist))
# cv.imshow('roimask, edges, bw, dist', imgStack)



# Create the CV_8U version of the distance image
# It is needed for findContours()
dist_8u = dist.astype('uint8')

# Find total markers
contours, hierarchy = cv.findContours(dist_8u, cv.RETR_CCOMP, cv.CHAIN_APPROX_SIMPLE)
print('total contours', len(contours))

# Create the marker image for the watershed algorithm
markers = np.zeros(dist.shape, dtype=np.int32)
# markers = np.zeros(dist.shape, dtype=np.int8)


# Draw the foreground markers
for i in range(len(contours)):
            cv.drawContours(markers, contours, i, (i+1))


# Draw the background marker
# cv.circle(markers, (5,5), 3, (255,255,255), -1)
# cv.imshow('Markers', markers.astype('uint8'))

# Perform the watershed algorithm
cv.watershed(imgResult, markers)
# cv.watershed(imgResult, markers.astype('int32'))

#mark = np.zeros(markers.shape, dtype=np.uint8)
mark = markers.astype('uint8')
# mark = cv.bitwise_and(mark, roimask)
mark = cv.bitwise_and(mark, edges)
# cv.imshow('Markers_v2', mark)

out = mark
# out = markers

# Generate random colors
colors = []
for contour in contours:
    colors.append((rng.randint(0,256), rng.randint(0,256), rng.randint(0,256)))

# Create the result image
dst = np.zeros((out.shape[0], out.shape[1], 3), dtype=np.uint8)

# Fill labeled objects with random colors
for i in range(out.shape[0]):
    for j in range(out.shape[1]):
        index = out[i,j]
        if index > 0 and index <= len(contours):
            dst[i,j,:] = colors[index-1]


# Visualize the final image
imgStack2 = np.hstack((imgResult, dst))
# cv.imshow('sharp, colorsegmentation', imgStack2)



cv.waitKey(0)





""" to fill area under edges """
# You can use fillcontour and hierarchy or search for connectedComponents with negative image
# https://www.learnopencv.com/filling-holes-in-an-image-using-opencv-python-c/







# """ trying to create point cloud using depth roi extracted onigiri """
# """ but I think I also need to do same roi extraction of color image as well """
# # http://www.open3d.org/docs/release/tutorial/Basic/rgbd_image.html

# cv.imwrite(path + '../../maskOnigiri.png', dst)

# color_raw = o3d.io.read_image(path + onigiriFolder + 'image.png')
# depth_raw = o3d.io.read_image(path + '../../maskOnigiri.png')

# rgbd_image = o3d.geometry.RGBDImage.create_from_color_and_depth(color_raw, depth_raw)
# print(rgbd_image)
# pcd = o3d.geometry.PointCloud.create_from_rgbd_image(
#         rgbd_image,
#         o3d.camera.PinholeCameraIntrinsic(
#             o3d.camera.PinholeCameraIntrinsicParameters.PrimeSenseDefault))
# # # Flip it, otherwise the pointcloud will be upside down
# pcd.transform([[1, 0, 0, 0], [0, -1, 0, 0], [0, 0, -1, 0], [0, 0, 0, 1]])
# o3d.visualization.draw_geometries([pcd])
