""" https://www.pynote.info/entry/opencv-findcontours """

import os
import sys
import cv2
import getpass
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Polygon


username = getpass.getuser()
osName = os.name
if osName == 'posix':
    os.system('clear')
else:
    os.system('cls')


# path to dir
path = os.path.dirname(__file__) + "/../images/onigiriTx/images/"

# Load the image. 
img = cv2.imread(path + '40.png')










# # Convert to grayscale.
# gray = cv2.cvtColor (img, cv2.COLOR_BGR2GRAY)
# ret, bw_img = cv2.threshold(gray, 100, 255, cv2.THRESH_BINARY)
# imb = bw_img

# # cv2.imshow('binary', bw_img)
# # cv2.waitKey(5000); cv2.destroyAllWindows()

# # Extract the contour.
# """contours, hierarchy = cv2.findContours (image, mode, method [, contours [, hierarchy [, offset]]])"""
# # contours, hierarchy = cv2.findContours (imb, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
# # contours, hierarchy = cv2.findContours (imb, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
# contours, hierarchy = cv2.findContours (imb, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
# # contours, hierarchy = cv2.findContours (imb, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)

# num_contours = len (hierarchy [ 0 ])

# # Create a tree based on the hierarchical structure with # any tree.

# #Create vertices. 
# root = Node ( "root" )
# nodes = {i: Node ("contour {i}" ) for i in  range (num_contours)}
# nodes [ -1 ] = root

# # Create an edge. 
# for i, (next_sibling, prev_sibling, first_child, parent) in  enumerate (hierarchy [ 0 ]):
#     nodes [i] .parent = nodes [parent]
#     pass
#     print (f"contour {i} (next_sibling: {next_sibling}, prev_sibling: {prev_sibling}," f"first_child: {first_child}, parent: {parent})")

# # Print the tree. 
# for pre, fill, node in RenderTree (root):
#     pass
#     #  print ( "{} {}" .format (pre, node.name))
     
# def  draw_contours (ax, img, contours):
#     ax.imshow (img)   # Show the image.
#     ax.set_axis_off ()

#     for i, cnt in  enumerate (contours):
#         # Change the shape. (NumPoints, 1, 2)-> (NumPoints, 2) 
#         cnt = cnt.squeeze (axis = 1 )
#          # Draw a line connecting contour points. 
#         ax.add_patch (Polygon (cnt, color = "b" , fill = None , lw = 2 ))
#          # Draw the contour points. 
#         ax.plot (cnt [:, 0 ], cnt [:, 1 ], "ro" , mew = 0 , ms = 4 )
#          # Draw the contour number. 
#         ax.text (cnt [ 0 ] [ 0 ], cnt [ 0 ] [ 1], i, color = "orange" , size = "20" )


# fig, ax = plt.subplots (figsize = ( 8 , 8 ))
# ax.set_title ( "cv2.RETR_EXTERNAL" )
# draw_contours (ax, img, contours)

# plt.show ()










# Draw the result of contour extraction in each method.
params = {
    "Cv2.CHAIN_APPROX_NONE" : cv2.CHAIN_APPROX_NONE,
     "Cv2.CHAIN_APPROX_SIMPLE" : cv2.CHAIN_APPROX_SIMPLE,
     "Cv2.CHAIN_APPROX_TC89_L1" : cv2.CHAIN_APPROX_TC89_L1,
     "Cv2.CHAIN_APPROX_TC89_KCOS" : cv2.CHAIN_APPROX_TC89_KCOS,
}

def  draw_contours (ax, img, contours):
    ax.imshow (img)   # Show the image.
    ax.set_axis_off ()

    for i, cnt in  enumerate (contours):
        # Change the shape. (NumPoints, 1, 2)-> (NumPoints, 2) 
        cnt = cnt.squeeze (axis = 1 )
         # Draw a line connecting contour points. 
        ax.add_patch (Polygon (cnt, color = "b" , fill = None , lw = 2 ))
         # Draw the contour points. 
        ax.plot (cnt [:, 0 ], cnt [:, 1 ], "ro" , mew = 0 , ms = 4 )
         # Draw the contour number. 
        # ax.text (cnt [ 0 ] [ 0 ], cnt [ 0 ] [ 1], i, color = "orange" , size = "20" )


fig = plt.figure (figsize = ( 8 , 8 ))
for i, (name, param) in  enumerate (params.items (), 1 ):

    # Convert to grayscale.
    gray = cv2.cvtColor (img, cv2.COLOR_BGR2GRAY)
    ret, bw_img = cv2.threshold(gray, 100, 255, cv2.THRESH_BINARY)
    imb = bw_img

    # cv2.imshow('binary', bw_img)
    # cv2.waitKey(5000); cv2.destroyAllWindows()

    # Extract the contour.
    contours, hierarchy = cv2.findContours (imb, cv2.RETR_EXTERNAL, param)
    # contours, hierarchy = cv2.findContours (imb, cv2.RETR_TREE, param)
    # contours, hierarchy = cv2.findContours (imb, cv2.RETR_LIST, param)
    # contours, hierarchy = cv2.findContours (imb, cv2.RETR_CCOMP, param)


    # Draw the extracted contour. 
    ax = fig.add_subplot ( 2 , 2 , i)
    ax.set_title(f"method = {name}")
    draw_contours(ax, img, contours)

plt.show ()
