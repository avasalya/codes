""" stream using realsense and detect onigiri using yolo"""
# -*- coding: utf-8 -*-

import os
import time
import getpass
import argparse
import cv2 as cv
import numpy as np
import random as rng
import pyrealsense2 as rs



username = getpass.getuser()
osName = os.name
if osName == 'posix':
    os.system('clear')
else:
    os.system('cls')


# # onigiri network parameters
parser = argparse.ArgumentParser(add_help=False)
parser.add_argument("--weights", default='yolov3/yolov3-onigiri.backup', help="YOLO weights path")
parser.add_argument("--config", default='yolov3/yolov3-onigiri.cfg', help="YOLO config path")
parser.add_argument("--names", default='yolov3/onigiri.names', help="class names path")
args = parser.parse_args()


""" https://www.pyimagesearch.com/2015/02/16/faster-non-maximum-suppression-python/ """

def nms(box, confThresh=None, overlapThresh=0.3):

    # if there are no boxes, return an empty list
    if len(box) == 0:
        print("no box found")
        return []

    # # if the bounding boxes integers, convert them to floats --
    # # this is important since we'll be doing a bunch of divisions
    boxes = np.array(box)

    # initialize the list of picked indexes	
    pick = []

    # grab the coordinates of the bounding boxes
    x1 = boxes[:,0]
    y1 = boxes[:,1]
    x2 = boxes[:,2]
    y2 = boxes[:,3]

    # compute the area of the bounding boxes and sort the bounding
    # boxes by the bottom-right y-coordinate of the bounding box
    area = (x2 - x1 + 1) * (y2 - y1 + 1)
    idxs = y2
    
    # if confidence probabilities are provided, sort on them instead
    if confThresh is not None:
        idxs = confThresh

    # sort the indexes
    idxs = np.argsort(idxs)
    
    # keep looping while some indexes still remain in the indexes list
    while len(idxs) > 0:

        # grab the last index in the indexes list and add the
        # index value to the list of picked indexes
        last = len(idxs) - 1
        i = idxs[last]
        pick.append(i)

        # find the largest (x, y) coordinates for the start of
        # the bounding box and the smallest (x, y) coordinates
        # for the end of the bounding box
        xx1 = np.maximum(x1[i], x1[idxs[:last]])
        yy1 = np.maximum(y1[i], y1[idxs[:last]])
        xx2 = np.minimum(x2[i], x2[idxs[:last]])
        yy2 = np.minimum(y2[i], y2[idxs[:last]])

        # compute the width and height of the bounding box
        w = np.maximum(0, xx2 - xx1 + 1)
        h = np.maximum(0, yy2 - yy1 + 1)

        # compute the ratio of overlap
        overlap = (w * h) / area[idxs[:last]]
                
        # delete all indexes from the index list that have
        idxs = np.delete(idxs, np.concatenate(([last], np.where(overlap > overlapThresh)[0])))
        # return only the bounding boxes that were picked using the
        # integer data type
    return boxes[pick].astype("int")




if __name__ == '__main__':

    CONF_THRESH, NMS_THRESH = 0.8, 0.8
    drawBbox = False
    
    # Load the network
    net = cv.dnn.readNetFromDarknet(args.config, args.weights)
    net.setPreferableBackend(cv.dnn.DNN_BACKEND_CUDA) # DNN_BACKEND_OPENCV
    net.setPreferableTarget(cv.dnn.DNN_TARGET_CUDA) # DNN_TARGET_CPU

    # Stream (Color/Depth) settings
    config = rs.config()
    config.enable_stream(rs.stream.color, 640 , 480 , rs.format.bgr8, 60)
    config.enable_stream(rs.stream.depth, 640 , 480 , rs.format.z16, 60)
    config.enable_stream(rs.stream.infrared, 640 , 480 , rs.format.y8, 60)


    # Start streaming
    pipeline = rs.pipeline()
    profile = pipeline.start(config)

    try:
        while  True:
            # Wait for frame (Color & Depth)
            frames = pipeline.wait_for_frames()
            
            color_frame = frames.get_color_frame()
            # color_frame = rs.align(rs.stream.depth).process(frames).get_color_frame()

            # depth_frame = frames.get_depth_frame()
            depth_frame = rs.align(rs.stream.color).process(frames).get_depth_frame()
            
            
            if  not depth_frame or  not color_frame:
                continue
            color_image = np.asanyarray(color_frame.get_data())

            # Depth image
            depth_color_frame = rs.colorizer().colorize(depth_frame)
            depth = np.asanyarray(depth_color_frame.get_data())
            
            # Read and convert the image to blob and perform forward pass 
            # to get the bounding boxes with their confidence scores
            rgb = color_image
            height, width = rgb.shape[:2]

    
            # Get the output layer from YOLO
            layers = net.getLayerNames()
            output_layers = [layers[i[0] - 1] for i in net.getUnconnectedOutLayers()]

            blob = cv.dnn.blobFromImage(rgb, 0.00392, (384, 384), swapRB=True, crop=False)
            # blob = cv.dnn.blobFromImage(rgb, 0.00392, (416, 416), swapRB=True, crop=False)
            net.setInput(blob)
            start = time.time()
            layer_outputs = net.forward(output_layers)
            end = time.time()

            # show timing information on YOLO
            print("[INFO] YOLO took {:.6f} seconds".format(end - start))

            class_ids, confidences, boxes = [], [], []

            for output in layer_outputs:

                for detection in output:
                    scores = detection[5:]
                    class_id = np.argmax(scores)
                    confidence = scores[class_id]

                    if confidence > CONF_THRESH:

                        center_x, center_y, w, h = (detection[0:4] * np.array([width, height, width, height])).astype('int')
                        
                        x = int(center_x - w / 2)
                        y = int(center_y - h / 2)

                        boxes.append([x, y, int(w), int(h)])
                        confidences.append(float(confidence))
                        class_ids.append(int(class_id))

            # print("confidence", confidences)
            # print("total boxes", len(boxes))

            # # Draw the filtered bounding boxes with their class to the image
            with open(args.names, "r") as f:
                classes = [line.strip() for line in f.readlines()]
            # colors = np.random.uniform(0, 255, size=(len(classes), 3))
            color = (255,0,255)

            CV_NMS = False
            if CV_NMS:
                # Perform non maximum suppression for the bounding boxes to filter overlapping and low confident bbox
                indices = cv.dnn.NMSBoxes(boxes, confidences, CONF_THRESH, NMS_THRESH)
                # print("indices", indices)

                # draw bounding boxes
                if len(indices)>0:
                    for idx in indices.flatten():
                        (x, y) = (boxes[idx][0], boxes[idx][1])
                        (w, h) = (boxes[idx][2], boxes[idx][3])

                        cv.rectangle(rgb, (x, y), (x + w, y + h), color, 1)
                        cv.rectangle(depth, (x, y), (x + w, y + h), color, 1)
                        cv.putText(rgb, classes[class_ids[idx]], (x, y -5), cv.FONT_HERSHEY_COMPLEX_SMALL, 1, color, 2)
                        cv.putText(depth, classes[class_ids[idx]], (x, y -5), cv.FONT_HERSHEY_COMPLEX_SMALL, 1, color, 2)

                images = np.hstack((rgb, depth))

            else:
                # Perform non maximum suppression for the bounding boxes to filter overlapping and low confident bbox
                pick = nms(boxes, confidences, NMS_THRESH)
                # print("after applying non-maximum, total bounding boxes", len(boxes), "reduced to", (len(pick)))
                
                # draw bounding boxes
                i = 0
                x_, y_, w_, h_ = [], [], [], []
                
                for (x, y, w, h) in pick:
                    
                    if drawBbox:
                        cv.rectangle(rgb, (x, y), (x + w, y + h), color, 1)
                        cv.rectangle(depth, (x, y), (x + w, y + h), color, 1)
                    else:
                        pass
                    
                    if i < len(pick):
                        if drawBbox:
                            cv.putText(rgb, classes[class_ids[i]], (x, y-5), cv.FONT_HERSHEY_COMPLEX_SMALL, 1, color, 1)
                            cv.putText(depth, classes[class_ids[i]], (x, y-5), cv.FONT_HERSHEY_COMPLEX_SMALL, 1, color, 1)
                        else:
                            pass
                            
                        x_.append(x)
                        y_.append(y)
                        w_.append(w)
                        h_.append(h)

                        i+=1


                """ background removal """
                roi = np.zeros(rgb.shape, dtype='uint8')
                roid = np.zeros(depth.shape, dtype='uint8')
                for i in range(len(x_)):
                    
                    # create white mask
                    roi[y_[i] : y_[i] + h_[i], x_[i] : (x_[i] + w_[i])] = 255
                    roid[y_[i] : y_[i] + h_[i], x_[i] : (x_[i] + w_[i])] = 255
                
                maskrgb = cv.bitwise_and(rgb, roi)
                maskdepth = cv.bitwise_and(depth, roid)
                    

            """ background blur """
            kernel = (11, 41)
            blur = cv.GaussianBlur(rgb, kernel, 0)
            blur = cv.bitwise_or(maskrgb, blur)

            images = np.hstack((rgb, depth, maskrgb, maskdepth, blur))

            
            # """ visualization """
            cv.namedWindow( 'onigiri' ,cv.WINDOW_AUTOSIZE)
            cv.imshow( 'onigiri' ,images)
            
            key = cv.waitKey(1) & 0xFF
            
            if  key == 27:
                break
            elif key == ord("b"):
                drawBbox = True
            elif key == ord("B"):
                drawBbox = False

    finally:
        # Stop streaming
        print("stopping streaming...")
        pipeline.stop()
        cv.destroyAllWindows()

