# -*- coding: utf-8 -*-

""" https://docs.opencv.org/master/d7/d1c/tutorial_js_watershed.html """ #cpp
""" https://docs.opencv.org/master/d3/db4/tutorial_py_watershed.html """ #py



# good explaination
""" http://www.cmm.mines-paristech.fr/~beucher/wtshed.html """

# try this 
""" https://www.pyimagesearch.com/2015/11/02/watershed-opencv/ """

import os
import sys
import cv2 as cv
import getpass
import numpy as np
import matplotlib.pyplot as plt


username = getpass.getuser()
osName = os.name
if osName == 'posix':
    os.system('clear')
else:
    os.system('cls')

path = os.path.dirname(__file__) + '/onigiriGrid/'


#reading the image
image = cv.imread(path + 'b/11/image.png')
# image = cv.imread(path + 'a/11/image.png', cv.IMREAD_COLOR)
print('original', image.shape)
print('original', image.dtype)


#converting image to grayscale format
gray = cv.cvtColor(image,cv.COLOR_BGR2GRAY)


#apply thresholding
# ret, thresh = cv.threshold(gray, 0 , 255, cv.THRESH_BINARY_INV + cv.THRESH_OTSU)
# thresh = cv.adaptiveThreshold(gray,255,cv.ADAPTIVE_THRESH_MEAN_C,cv.THRESH_BINARY,9,2)
thresh = cv.adaptiveThreshold(gray,255,cv.ADAPTIVE_THRESH_GAUSSIAN_C,cv.THRESH_BINARY_INV,11,2)
  

#get a kernel
kernel = np.ones((3,3), np.uint8)
opening = cv.morphologyEx(thresh, cv.MORPH_OPEN, kernel,iterations = 1)
# cv.imshow('thresh', opening)


#sure background area in the image
sure_bg = cv.dilate(opening, kernel, iterations = 3)
# cv.imshow('bg', sure_bg)

# find sure foreground area
dist_transform = cv.distanceTransform(opening,cv.DIST_L2, 5)
# cv.imshow('dist_transform', dist_transform)

ret,sure_fg = cv.threshold(dist_transform, 0.1*dist_transform.max(), 255, 0)
# cv.imshow('fg', sure_fg)

# find unknown region
sure_fg = np.uint8(sure_fg)
unknown = cv.subtract(sure_bg,sure_fg)
# cv.imshow('unknown', unknown)

# Marker labelling
ret,markers = cv.connectedComponents(sure_fg)

# Add one to all labels so that sure background is not 0, but 1
markers = markers+1

# Now, mark the region of unknown with zero
markers[unknown==255] = 0

markers = cv.watershed(image,markers)
image[markers==-1] = [255,0,0]


res = np.hstack((thresh, sure_bg, dist_transform, sure_fg, unknown))
cv.imshow('thresh, bg, dist_transform, fg, unknown', res)
cv.waitKey()