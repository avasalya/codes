# -*- coding: utf-8 -*-

""" histogram backprojection """


from __future__ import print_function
from __future__ import division


import os
import sys
import cv2 as cv
import numpy as np
import argparse
import getpass
import matplotlib.pyplot as plt 

username = getpass.getuser()
osName = os.name
if osName == 'posix':
    os.system('clear')
else:
    os.system('cls')

path = os.path.dirname(__file__) + '/onigiriGrid/'

# print('cv version', cv.__version__)


""" https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_imgproc/py_histograms/py_histogram_backprojection/py_histogram_backprojection.html#histogram-backprojection """

""" using manual object ROI """


# """ read rgb image """
# img = cv.imread(path + 'b/11/image.png')
# # cv2.imshow('using cv2', img)
# # cv2.waitKey(5000); cv2.destroyAllWindows()

# # target is the image we search in
# target = img
# hsv =  cv.cvtColor(target, cv.COLOR_BGR2HSV)
# # cv.imshow('target', target)

# # roi is the object or region of object we need to find
# roi = cv.imread('onigiri_masked.jpg')
# # roi = cv.imread('onigiri_cropped.png')
# hsvt = cv.cvtColor(roi, cv.COLOR_BGR2HSV)
# # cv.imshow('roi', roi)

# # calculating object histogram
# # roiHist    = cv.calcHist([hsv],[0, 1], None, [180, 256], [0, 180, 0, 256] )
# # targetHist = cv.calcHist([hsvt],[0, 1], None, [180, 256], [0, 180, 0, 256] )

# roiHist    = cv.calcHist([hsv],[0], None, [256], [1, 256] )
# targetHist = cv.calcHist([hsvt],[0], None, [256], [1, 256] )

# # normalize histogram
# cv.normalize(roiHist,roiHist,0,255,cv.NORM_MINMAX)

# # apply backprojection
# # backproj = cv.calcBackProject([hsvt],[0,1],roiHist,[0,180,0,256],1)
# backproj = cv.calcBackProject([hsvt], [1], roiHist, [0,256], 1)
# # backproj = cv.calcBackProject([hsvt], [0], roiHist, [0, 180], 1)

# # Now convolute with circular disc
# disc = cv.getStructuringElement(cv.MORPH_ELLIPSE,(5,5))
# # plt.plot(disc)
# # plt.show()
# con2d = cv.filter2D(backproj,-1,disc,backproj)

# # threshold and binary AND
# # ret,thresh = cv.threshold(backproj,50,255,0)
# ret,thresh = cv.threshold(con2d,50,255,0, cv.THRESH_BINARY)
# thresh = cv.merge((thresh,thresh,thresh))

# # cv.imshow('roi binary mask', thresh)
# print(target.shape)
# print(thresh.shape)
# bitand = cv.bitwise_and(target, thresh)

# # cv.imshow('roi extracted', bitand)

# bitand = np.hstack((target, roi, thresh, bitand))
# cv.imshow('target, roi, roi-binary, roi-extracted', bitand)
# # cv.imwrite('bitand.jpg',bitand)


# cv.waitKey(); cv.destroyAllWindows()
















""" https://docs.opencv.org/3.4/da/d7f/tutorial_back_projection.html """






""" adaptive threshold with contours """


def Hist_and_Backproj(val):
    
    bins = val
    histSize = max(bins, 2)
    
    # hue_range
    ranges = [0, 180]
    # ranges = [0, 256]
    
    # calculating object histogram
    hist = cv.calcHist([hue], [0], None, [histSize], ranges, accumulate=False)
    
    # normalize histogram
    cv.normalize(hist, hist, alpha=0, beta=255, norm_type=cv.NORM_MINMAX)
    
            
    w = 480
    h = 640
    bin_w = int(round(w / histSize))
    histImg = np.zeros((h, w, 3), dtype=np.uint8)
    for i in range(bins):
        cv.rectangle(histImg, (i*bin_w, h), ( (i+1)*bin_w, h - int(np.round(hist[i]*h/255.0 )) ), (0, 0, 255), cv.FILLED)
    # cv.imshow('Histogram', histImg)
    
    
    # apply backprojection
    # backproj = cv.calcBackProject([hue], [0,1], hist, [0,180,0,256],1)
    backproj = cv.calcBackProject([hue], [0], hist, ranges, scale=1)
    backprojmerge = cv.merge((backproj, backproj, backproj))
    # cv.imshow('BackProj', backproj)
    

    # threshold
    # thresh = cv.threshold(backproj, 50, 255, 0, cv.THRESH_BINARY)
    # thresh = cv.adaptiveThreshold(backproj,255,cv.ADAPTIVE_THRESH_MEAN_C,cv.THRESH_BINARY,9,2)
    thresh = cv.adaptiveThreshold(backproj,255,cv.ADAPTIVE_THRESH_GAUSSIAN_C,cv.THRESH_BINARY,11,2)
    
    thresh3x = cv.merge((thresh, thresh, thresh))
    

    
    # binary operations
    # bitand = cv.bitwise_and(img, img, mask = thresh)    
    bitand = cv.bitwise_and(img, thresh3x)
    # # cv.imshow('bitwise and', bitand)


    #calculate the contours from binary image
    contours, hierarchy = cv.findContours(thresh, cv.RETR_CCOMP, cv.CHAIN_APPROX_SIMPLE) 
    with_contours = cv.drawContours(img, contours, -1, (0, 255, 0), 3) 
    

    res = np.hstack((img, backprojmerge, thresh3x, bitand, with_contours))
    cv.imshow('source, backproj, thresh, binary, with_contours', res)

    return res



img = cv.imread(path + 'b/11/image.png')
# img = cv.imread(path + 'e/43/image.png')
# img = cv.imread(path + 'a/50/image.png')

# cv.imshow('using cv', img)
# cv.waitKey(5000); cv.destroyAllWindows()

hsv = cv.cvtColor(img, cv.COLOR_BGR2HSV)
ch = (0, 0)
hue = np.empty(hsv.shape, hsv.dtype)
cv.mixChannels([hsv], [hue], ch)

bins = 0
window_image = 'Source image'
cv.namedWindow(window_image)
cv.createTrackbar('* Hue  bins: ', window_image, bins, 180, Hist_and_Backproj )

res = Hist_and_Backproj(bins)

cv.imshow(window_image, img)
cv.waitKey()



""" https://realpython.com/python-opencv-color-spaces/ """
# use cv2.inRange() to try to threshold Nemo. inRange() takes three parameters: the image, the lower range, and the higher range. It returns a binary mask (an ndarray of 1s and 0s) the size of the image where values of 1 indicate values within the range, and zero values indicate values outside:


# mask = cv.inRange(hsv, light_orange, dark_orange)
















""" using hue """


# def Hist_and_Backproj(val):
    
#     bins = val
#     histSize = max(bins, 2)
#     ranges = [0, 180]  # [0,256] # hue_range
    
#     # calculating object histogram
#     hist = cv.calcHist([hue], [0], None, [histSize], ranges, accumulate=False)
    
#     # normalize histogram
#     cv.normalize(hist, hist, alpha=0, beta=255, norm_type=cv.NORM_MINMAX)
    
            
#     w = 480
#     h = 640
#     bin_w = int(round(w / histSize))
#     histImg = np.zeros((h, w, 3), dtype=np.uint8)
#     for i in range(bins):
#         cv.rectangle(histImg, (i*bin_w, h), ( (i+1)*bin_w, h - int(np.round(hist[i]*h/255.0 )) ), (0, 0, 255), cv.FILLED)
#    # cv.imshow('Histogram', histImg)
    
    
#     # apply backprojection
#     # backproj = cv.calcBackProject([hue], [0,1], hist, [0,180,0,256],1)
#     backproj = cv.calcBackProject([hue], [0], hist, ranges, scale=1)
#     backprojmerge = cv.merge((backproj, backproj, backproj))
#     # cv.imshow('BackProj', backproj)
    

#     # Now convolute with circular disc
#     disc = cv.getStructuringElement(cv.MORPH_ELLIPSE,(5,5))
#     con2d = cv.filter2D(backproj, -1, disc, backproj)
    
#     # threshold
#     # ret,thresh = cv.threshold(backproj,50,255,0)
#     ret,thresh = cv.threshold(con2d, 50, 255, 0, cv.THRESH_BINARY)
#     thresh3d = cv.merge((thresh, thresh, thresh))
    
#     # binary operations    
#     bitand = cv.bitwise_and(img, backprojmerge)
#     bitand = cv.bitwise_xor(img, bitand)
#     # cv.imshow('bitwise and', bitand)


#     # res = np.hstack((img, thresh3d, bitand))
#     res = np.hstack((img, backprojmerge, bitand))
#     cv.imshow('source, backproj, binary', res)

#     return thresh, backproj, bitand



# """ read rgb image """
# img = cv.imread(path + 'b/11/image.png')
# # img = cv.imread(path + 'e/43/image.png')
# # img = cv.imread(path + 'a/50/image.png')

# # cv.imshow('using cv', img)
# # cv.waitKey(5000); cv.destroyAllWindows()

# hsv = cv.cvtColor(img, cv.COLOR_BGR2HSV)
# ch = (0, 0)
# hue = np.empty(hsv.shape, hsv.dtype)
# cv.mixChannels([hsv], [hue], ch)

# bins = 0
# window_image = 'Source image'
# cv.namedWindow(window_image)
# cv.createTrackbar('* Hue  bins: ', window_image, bins, 180, Hist_and_Backproj )

# thresh, backproj, bitand = Hist_and_Backproj(bins)

# cv.imshow(window_image, img)


# # cv.imwrite('images/binary.jpg', thresh)
# # cv.imwrite('images/bitand.jpg', bitand)



# cv.waitKey(1000)












