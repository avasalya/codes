""" stream using realsense and detect onigiri using yolo"""
# -*- coding: utf-8 -*-

import time
import argparse
import cv2 as cv
import numpy as np
import pyrealsense2 as rs


# # onigiri network parameters
parser = argparse.ArgumentParser(add_help=False)
parser.add_argument("--weights", default='yolov3/yolov3-onigiri.backup', help="YOLO weights path")
parser.add_argument("--config", default='yolov3/yolov3-onigiri.cfg', help="YOLO config path")
parser.add_argument("--names", default='yolov3/onigiri.names', help="class names path")
args = parser.parse_args()

# # convini network parameters
# parser = argparse.ArgumentParser(add_help=False)
# parser.add_argument("--weights", default='convini/yolov3_convini2.weights', help="YOLO weights path")
# parser.add_argument("--config", default='convini/yolov3_convini2.cfg', help="YOLO config path")
# parser.add_argument("--names", default='convini/convini.names', help="class names path")
# args = parser.parse_args()


if __name__ == '__main__':

    CONF_THRESH, NMS_THRESH = 0.8, 0.5

    # Load the network
    net = cv.dnn.readNetFromDarknet(args.config, args.weights)
    net.setPreferableBackend(cv.dnn.DNN_BACKEND_CUDA) # DNN_BACKEND_OPENCV
    net.setPreferableTarget(cv.dnn.DNN_TARGET_CUDA) # DNN_TARGET_CPU

    # Stream (Color/Depth) settings
    config = rs.config()
    config.enable_stream(rs.stream.color, 640 , 480 , rs.format.bgr8, 60)
    config.enable_stream(rs.stream.depth, 640 , 480 , rs.format.z16, 60)
    config.enable_stream(rs.stream.infrared, 640 , 480 , rs.format.y8, 60)

    # Start streaming
    pipeline = rs.pipeline()
    profile = pipeline.start(config)

    try:
        while  True:
            # Wait for frame (Color & Depth)
            frames = pipeline.wait_for_frames()
            color_frame = frames.get_color_frame()
            depth_frame = frames.get_depth_frame()
            
            # depth_profile = depth_frame.get_profile()
            # print("fps", depth_profile.fps())

            if  not depth_frame or  not color_frame:
                continue
            color_image = np.asanyarray(color_frame.get_data())

            # Depth image
            depth_color_frame = rs.colorizer().colorize(depth_frame)
            depth_color_image = np.asanyarray(depth_color_frame.get_data())

            # Read and convert the image to blob and perform forward pass to get the bounding boxes with their confidence scores
            img = color_image
            height, width = img.shape[:2]

            # Get the output layer from YOLO
            layers = net.getLayerNames()
            output_layers = [layers[i[0] - 1] for i in net.getUnconnectedOutLayers()]

            blob = cv.dnn.blobFromImage(img, 0.00392, (416, 416), swapRB=True, crop=False)
            net.setInput(blob)
            start = time.time()
            layer_outputs = net.forward(output_layers)
            end = time.time()

            # show timing information on YOLO
            print("[INFO] YOLO took {:.6f} seconds".format(end - start))

            class_ids, confidences, boxes = [], [], []

            for output in layer_outputs:

                for detection in output:

                    scores = detection[5:]
                    class_id = np.argmax(scores)
                    confidence = scores[class_id]

                    if confidence > CONF_THRESH:

                        center_x, center_y, w, h = (detection[0:4] * np.array([width, height, width, height])).astype('int')

                        x = int(center_x - w / 2)
                        y = int(center_y - h / 2)

                        boxes.append([x, y, int(w), int(h)])
                        confidences.append(float(confidence))
                        class_ids.append(int(class_id))

                        
            # print("confidence", confidences)
            print("boxes", len(boxes))

            # # Draw the filtered bounding boxes with their class to the image
            with open(args.names, "r") as f:
                classes = [line.strip() for line in f.readlines()]
            # colors = np.random.uniform(0, 255, size=(len(classes), 3))
            color = (255,0,255)

            # Perform non maximum suppression for the bounding boxes to filter overlapping and low confident bounding boxes
            indices = cv.dnn.NMSBoxes(boxes, confidences, CONF_THRESH, NMS_THRESH)
            # print("indices", indices)

            # draw bounding boxes
            if len(indices)>0:
                for idx in indices.flatten():
                    (x, y) = (boxes[idx][0], boxes[idx][1])
                    (w, h) = (boxes[idx][2], boxes[idx][3])

                    # cv.rectangle(img, (x, y), (x + w, y + h), colors[idx], 2)
                    cv.rectangle(img, (x, y), (x + w, y + h), color, 1)
                    cv.putText(img, classes[class_ids[idx]], (x, y -5), cv.FONT_HERSHEY_COMPLEX_SMALL, 1, color, 2)
                    
            images = np.hstack((img, depth_color_image))
            cv.namedWindow( 'RealSense' ,cv.WINDOW_AUTOSIZE)
            cv.imshow( 'RealSense' ,images)
            # cv.imshow("image", img)
            if cv.waitKey( 1 ) & 0xff == 27 :
                break

    finally:
        # Stop streaming
        print("stopping streaming...")
        pipeline.stop()
        cv.destroyAllWindows()


