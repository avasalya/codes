#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import rospy
import getpass
import cv2 as cv
import cv_bridge
import numpy as np
from sensor_msgs.msg import Image,CameraInfo
from cv_bridge import CvBridge, CvBridgeError
import pyrealsense2 as rs




# clean terminal in the beginning
username = getpass.getuser()
osName = os.name
if osName == 'posix':
    os.system('clear')
else:
    os.system('cls')

    
def convert_depth_image(ros_image):
    
    bridge = CvBridge()
     # Use cv_bridge() to convert the ROS image to OpenCV format
   
    try: 
        #Convert the depth image using the default passthrough encoding
        # depth_image = bridge.imgmsg_to_cv2(ros_image, desired_encoding="bgr8")
        depth_image = bridge.imgmsg_to_cv2(ros_image, desired_encoding="passthrough")
        
        #Convert the depth image to a Numpy array
        depth_array = np.array(depth_image, dtype=np.float32)
        center_idx = np.array(depth_array.shape) / 2
        print ('center depth:', depth_array[center_idx[0], center_idx[1]])        
        
        cv.imshow('stream realsense', depth_image)
        # cv.imshow('stream realsense', depth_array)
        key = cv.waitKey(1) & 0xFF
        if  key == 27:
            rospy.loginfo("stopping streaming...")
            try:
                sys.exit(0)
            except SystemExit:
                os._exit(0)

    except:
        print("CvBridgeError")

def pixel2depth():
    
    rospy.init_node('pixel2depth',anonymous=True)
    
    rospy.Subscriber("/camera/aligned_depth_to_color/image_raw", Image,callback=convert_depth_image, queue_size=1)
    # rospy.Subscriber("/camera/color/image_raw", Image,callback=convert_depth_image, queue_size=1)
    
    rospy.spin()
    
    			


""" run this """
# roslaunch realsense2_camera rs_camera.launch align_depth:=true

if __name__ == '__main__':
    
    pixel2depth()