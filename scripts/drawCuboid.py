#%%
""" https://stackoverflow.com/questions/44881885/python-draw-parallelepiped """

import time
import math as m
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d.art3d import Poly3DCollection, Line3DCollection
from transformations import rotation_matrix, concatenate_matrices

#%%

# def plot_cube(cube_definition):
#     cube_definition_array = [
#         np.array(list(item))
#         for item in cube_definition
#     ]

#     Rx = rotation_matrix(m.pi/2, [1, 0, 0])
#     Ry = rotation_matrix(m.pi/2, [0, 1, 0])
#     Rz = rotation_matrix(m.pi/2, [0, 0, 1])
#     R = concatenate_matrices(Rx, Ry, Rz)[:3,:3]
#     print(cube_definition_array)

#     points = []
#     points += cube_definition_array
#     vectors = [
#         cube_definition_array[1] - cube_definition_array[0],
#         cube_definition_array[2] - cube_definition_array[0],
#         cube_definition_array[3] - cube_definition_array[0]
#     ]

#     points += [cube_definition_array[0] + vectors[0] + vectors[1]]
#     points += [cube_definition_array[0] + vectors[0] + vectors[2]]
#     points += [cube_definition_array[0] + vectors[1] + vectors[2]]
#     points += [cube_definition_array[0] + vectors[0] + vectors[1] + vectors[2]]

#     points = np.array(points)

#     edges = [
#         [points[0], points[3], points[5], points[1]],
#         [points[1], points[5], points[7], points[4]],
#         [points[4], points[2], points[6], points[7]],
#         [points[2], points[6], points[3], points[0]],
#         [points[0], points[2], points[4], points[1]],
#         [points[3], points[6], points[7], points[5]]
#     ]

#     print(np.array(edges).shape)

#     fig = plt.figure()
#     ax = fig.add_subplot(111, projection='3d')

#     faces = Poly3DCollection(edges, linewidths=1, edgecolors='k')
#     faces.set_facecolor((0,0,1,0.1))

#     ax.add_collection3d(faces)

#     # Plot the points themselves to force the scaling of the axes
#     ax.scatter(points[:,0], points[:,1], points[:,2], s=0)

#     # ax.set_aspect('equal')

# cube_definition = [(0,0,0), (0,1,0), (1,0,0), (0,0,1)]
# plot_cube(cube_definition)

# # %%


# def cube_coordinates(edge_len,step_size):
#     X = np.arange(0,edge_len+step_size,step_size)
#     Y = np.arange(0,edge_len+step_size,step_size)
#     Z = np.arange(0,edge_len+step_size,step_size)
#     temp=list()
#     for i in range(len(X)):
#         temp.append((X[i],0,0))
#         temp.append((0,Y[i],0))
#         temp.append((0,0,Z[i]))
#         temp.append((X[i],edge_len,0))
#         temp.append((edge_len,Y[i],0))
#         temp.append((0,edge_len,Z[i]))
#         temp.append((X[i],edge_len,edge_len))
#         temp.append((edge_len,Y[i],edge_len))
#         temp.append((edge_len,edge_len,Z[i]))
#         temp.append((edge_len,0,Z[i]))
#         temp.append((X[i],0,edge_len))
#         temp.append((0,Y[i],edge_len))

#     # print(np.array(temp).shape)
#     Rx = rotation_matrix(m.pi/4, [1, 0, 0])
#     Ry = rotation_matrix(m.pi/3, [0, 1, 0])
#     Rz = rotation_matrix(m.pi/2, [0, 0, 1])
#     R = concatenate_matrices(Rx, Ry, Rz)[:3,:3]
    
#     tempR = np.dot(np.array(temp), R)
    
#     return temp, tempR
    

# t1 = time.time()
# edge_len = 10
# A, tempR = cube_coordinates(edge_len,0.01)
# A=list(set(A))

# t2 = time.time()
# print("time to run func", t2-t1)


# fig = plt.figure()
# # ax = fig.add_subplot(111, projection='3d')
# # A=list(zip(*A))
# # X,Y,Z=list(A[0]),list(A[1]),list(A[2])
# # ax.scatter(X,Y,Z,c='g')

# tempR=list(zip(*tempR))
# X,Y,Z=list(tempR[0]),list(tempR[1]),list(tempR[2])
# # ax.scatter(X,Y,Z,c='b')

# plt.scatter(X,Y,Z, c='r')
# t3 = time.time()
# print("time to plot", t3-t1)
# plt.show()

# %%

edge = 10.
edges = np.array([  [-edge,-edge, edge],
                    [-edge,-edge,-edge],
                    [ edge,-edge,-edge],
                    [ edge,-edge, edge],
                    [-edge, edge, edge],
                    [-edge, edge,-edge],
                    [ edge, edge,-edge],
                    [ edge, edge, edge]])

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

cube=list(zip(*edges))
X,Y,Z=list(cube[0]),list(cube[1]),list(cube[2])
ax.scatter(X,Y,Z,c='b')
plt.show()
