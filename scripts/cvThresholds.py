""" https://www.analyticsvidhya.com/blog/2019/03/opencv-functions-computer-vision-python/ """

import os
import sys
import cv2
import getpass
import numpy as np

import matplotlib.pyplot as plt
from matplotlib.patches import Polygon


username = getpass.getuser()
osName = os.name
if osName == 'posix':
    os.system('clear')
else:
    os.system('cls')

path = os.path.dirname(__file__) + '/onigiriGrid/'



""" read rgb image """
# img = cv2.imread(path + '../rgb.png')
img = cv2.imread(path + 'b/11/image.png')
# img = cv2.imread(path + 'sample/image.png', cv2.CV_16UC1)
print('original shape', img.shape)
print('original max', img.max())
print('original min', img.min())

cv2.imshow('using cv2', img)
cv2.waitKey(5000); cv2.destroyAllWindows()



""" flatten the data dimensions from MxNx3 to MxN*3 """
# img = img.reshape(img.shape[0], -1)
# print(img[345,1000])
# print('after flattening', img.shape)



""" Convert to grayscale then to binary """
# gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
# ret, bw_img = cv2.threshold(gray, 100, 255, cv2.THRESH_BINARY)
# imb = bw_img
# cv2.imshow('binary', imb)
# cv2.waitKey(5000); cv2.destroyAllWindows()




""" load rbg as gray image """
gray_img = cv2.imread(path + 'b/11/image.png', 0)  # gray scale format
print('gray image shape', gray_img.shape)
print('gray image max', gray_img.max())
print('gray image min', gray_img.min())




""" Simple Image Thresholding """
ret, th_bin = cv2.threshold(gray_img, 100, 255, cv2.THRESH_BINARY)
ret, th_bin_inv = cv2.threshold(gray_img, 100, 255, cv2.THRESH_BINARY_INV)
ret, th_tozero = cv2.threshold(gray_img, 100, 255, cv2.THRESH_TOZERO)
ret, th_tozero_inv = cv2.threshold(gray_img, 100, 255, cv2.THRESH_TOZERO_INV)
ret, th_thunc = cv2.threshold(gray_img, 100, 255, cv2.THRESH_TRUNC)
ret, th_otsu = cv2.threshold(gray_img, 100, 255, cv2.THRESH_OTSU)

names = ['th_bin', 'th_bin_inv', 'th_tozero', 'th_tozero_inv', 'th_thunc', 'th_otsu']
images = th_bin, th_bin_inv, th_tozero, th_tozero_inv, th_thunc, th_otsu

plt1 = plt.figure(1)
for i in range(6):
    plt.subplot(2,3, i+1)
    plt.imshow(images[i], 'gray')
    plt.title(names[i])

plt.show(block=False)
plt.pause(5)
plt.close(plt1) 





