# -*- coding: UTF-8 -*-

import os
import getpass
import numpy as np
import open3d.open3d as o3d
import matplotlib.pyplot as plt
# from openni import openni2

# clear terminal ctrl+L
username = getpass.getuser()
print('using user-->', username)
osName = os.name

if osName == 'posix':
    
    if username  == 'ash':
        path = "/home/ash/pCloudDrive/TXcodes/onigiriGrid/"
    else:
        path = "/home/avasalya/pCloudDrive/TXcodes/onigiriGrid/"


    os.system('clear')
else:
    os.system('cls')



""" 
## linear
    float depth_in_meters = scale * d16
## inverse
    float depth_in_meters = scale / d16
 """

""" ex source 

http://www.open3d.org/docs/release/tutorial/Basic/rgbd_images/redwood.html

"""

if __name__ == "__main__":

    color_raw = o3d.io.read_image(path + "sample/image.png")
    depth_raw = o3d.io.read_image(path + "sample/depth.png")
    depth_viz = o3d.io.read_image(path + "sample/depth_viz.png")
    
    print(depth_raw)
    dep = np.asarray(depth_raw)
    print(dep)
    scale = 1000 # 100 # depth is in cm
    print('depth is ', dep[0, 0]/scale) # [y, x]/scale
    
    # plt.subplot(131)
    # plt.imshow(color_raw)
    # plt.subplot(132)
    # plt.imshow(depth_raw)
    # plt.subplot(133)
    # plt.imshow(depth_viz)
    # plt.show()
    
    # depth_raw = o3d.io.read_image(path + "../o1.png")
    # print(depth_raw)
    # dep = np.asarray(depth_raw)
    # print(dep.shape)
    # # scale = 1 # 24 # depth is in cm
    # print('o1 depth is ', dep[372, 459]*scale) # [y, x]/scale



    # color_raw = o3d.io.read_image(path + "depth1-rgb.png")
    # depth_raw = o3d.io.read_image(path + "../depth1.png")
    
    rgbd_image = o3d.geometry.RGBDImage.create_from_color_and_depth(color_raw, depth_raw)
    print(rgbd_image)

    # plt.subplot(1, 2, 1)
    # plt.title('synthetic onigiri grayscale image')
    # plt.imshow(rgbd_image.color)
    # plt.subplot(1, 2, 2)
    # plt.title('synthetic onigiri depth image')
    # plt.imshow(rgbd_image.depth)
    # plt.show()


    """ camera/image properties are not correct """
    # though this default parameters are also doing ok ok job
    pcd = o3d.geometry.PointCloud.create_from_rgbd_image(
        rgbd_image,
        o3d.camera.PinholeCameraIntrinsic(
            o3d.camera.PinholeCameraIntrinsicParameters.PrimeSenseDefault))
    # Flip it, otherwise the pointcloud will be upside down
    pcd.transform([[1, 0, 0, 0], [0, -1, 0, 0], [0, 0, -1, 0], [0, 0, 0, 1]])
    o3d.visualization.draw_geometries([pcd])
    
    
    # vis = o3d.visualization.Visualizer()
    # vis.create_window()
    # vis.add_geometry(pcd)
    # outdepth = vis.capture_depth_float_buffer()
    # # plt.imshow(np.asarray(outdepth))
    # # plt.show()
    # print(np.asarray(outdepth))
 

    # """ put astra/UE4 NDS camera properties here """
    # # camera_parameters = o3d.camera.PinholeCameraParameters()
    # # camera_parameters.extrinsic = np.array([[1,0,0,1],
    # #                                     [0,1,0,0],
    # #                                     [0,0,1,2],
    # #                                     [0,0,0,1]])
    # # camera_parameters.intrinsic.set_intrinsics(width=1920, height=1080, fx=1000, fy=1000, cx=959.5, cy=539.5)
