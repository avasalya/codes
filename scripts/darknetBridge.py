#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" real-time darknet-yolov4-masking RGB or Depth with realsense D435
    26/June/2020, Ashesh Vasalya, AIST
    adapted from http://wiki.ros.org/cv_bridge/Tutorials/ConvertingBetweenROSImagesAndOpenCVImagesPython
"""


""" getting started """
# $ roslaunch realsense2_camera rs_camera.launch align_depth:=true
# $ roslaunch darknet_ros onigiriYolov4.launch
# $ python darknetBridge.py


import os
import sys
import rospy
import getpass
import ros_numpy
import cv_bridge
import cv2 as cv
import numpy as np

from sensor_msgs.msg import Image,CameraInfo
from cv_bridge import CvBridge, CvBridgeError
from darknet_ros_msgs.msg import BoundingBoxes,BoundingBox


class Publishsers():
    
    def __init__(self, pubTopic):
        
        # Publisher
        self.publisher = rospy.Publisher(pubTopic, Image, queue_size = 100)
        self.stream = Image()
        self.roi = Image()

    def get_msg(self, Image, boxes, encoding):
        
        bridge = CvBridge()
        
        try:
            # stream image
            self.img = bridge.imgmsg_to_cv2(Image, encoding)
            cv.imshow("stream", self.img)

            #convert image to np array
            self.stream = np.array(self.img, dtype=np.float32)
            
            self.roi = np.zeros(self.stream.shape, np.uint8)
            
        except: 
            pass

        for i in boxes.bounding_boxes:

            if i.Class == "onigiri":
                try:
                    self.box_area = self.stream[i.ymin:i.ymax, i.xmin:i.xmax]
                    self.roi[i.ymin:i.ymax, i.xmin:i.xmax] = self.box_area
                except:
                    pass
    
        self.roimsg = bridge.cv2_to_imgmsg(self.roi, encoding)
        
        #Time stamp
        self.roimsg.header.stamp = rospy.Time.now()



    def send_msg(self):
        
        # publish
        try:
            self.publisher.publish(self.roimsg)
            
        except CvBridgeError, e:
                rospy.logerr("CvBridge Error: {0}".format(e))

        img = ros_numpy.numpify(self.roimsg)
        cv.imshow("masked", img)
                
        key = cv.waitKey(1) & 0xFF
        if  key == 27:
            rospy.loginfo("stopping streaming...")
            try:
                sys.exit(0)
            except SystemExit:
                os._exit(0)





class Subscribers():
    
    def __init__(self, publisher, imgType, encoding):
        
        self.encoding = encoding
        self.publisher = publisher
        self.image = Image()

        if imgType == 'color':
            # color image sub
            self.color_sub = rospy.Subscriber('/camera/color/image_raw', Image, self.callback, queue_size = 1)        
        elif imgType == 'depth':
            # depth image sub
            self.color_sub = rospy.Subscriber('/camera/aligned_depth_to_color/image_raw', Image, self.callback, queue_size = 1)
              
        # darknet bounding box sub
        self.bbox_sub = rospy.Subscriber("/darknet_ros/bounding_boxes", BoundingBoxes, self.bbox_callback, queue_size = 1)
                
 
 
    def callback(self, Image):
        
        self.image = Image
        

    def bbox_callback(self, boxes):
        
        self.publisher.get_msg(self.image, boxes, self.encoding)
        self.publisher.send_msg()
        
        


def main():

    # clean terminal in the beginning
    username = getpass.getuser()
    osName = os.name
    if osName == 'posix':
        os.system('clear')
    else:
        os.system('cls')    
    
    # init rosnode
    rospy.init_node('onigiriNode')
    rospy.loginfo("streaming now...")
    
    # calling subscriber and publisher
    publisher = Publishsers('/onigiri')
    Subscribers(publisher, 'depth', 'passthrough')
    # Subscribers(publisher, 'color', 'bgr8')

    # ros spin
    rospy.spin()

    # sleeprate
    rate = rospy.Rate(50)

    while not rospy.is_shutdown():
        publisher.send_msg()
        rate.sleep()



if __name__ == '__main__':

    main()
    