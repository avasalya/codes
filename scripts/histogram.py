import os
import sys
import cv2
import getpass
import numpy as np

import matplotlib.pyplot as plt

username = getpass.getuser()
print('using user-->', username)
osName = os.name

if osName == 'posix':
    
    if username  == 'ash':
        path = "/mnt/13dcd5af-8ac2-4e8d-ae07-d0f1a910ba54/ashDrive/Dropbox (国立研究開発法人 産業技術総合研究所)/Odaiba/TXcodes/onigiriGrid/"
    else:
        path = "/mnt/13dcd5af-8ac2-4e8d-ae07-d0f1a910ba54/ashDrive/Dropbox (国立研究開発法人 産業技術総合研究所)/Odaiba/TXcodes/onigiriGrid/"

    os.system('clear')
else:
    os.system('cls')



""" read rgb image """
img = cv2.imread(path + 'b/11/image.png')
print('original shape', img.shape)
print('original max', img.max())
print('original min', img.min())

# cv2.imshow('using cv2', img)
# cv2.waitKey(5000); cv2.destroyAllWindows()

""" load rbg as gray image """
gray_img = cv2.imread(path + 'b/11/image.png', 0)  # gray scale format
print('gray image shape', gray_img.shape)
print('gray image max', gray_img.max())
print('gray image min', gray_img.min())

# cv2.imshow('using cv2', gray_img)
# cv2.waitKey(5000); cv2.destroyAllWindows()




""" https://docs.opencv.org/master/d1/db7/tutorial_py_histogram_begins.html """

""" plot r, g, b histogram using plt"""
# color = ('b','g','r')
# for i,col in enumerate(color):
#     histr = cv2.calcHist([img],[i],None,[256],[1,256])
#     plt.plot(histr,color = col)
#     plt.xlim([0,256])
# plt.show()



""" plot histogram of gray image """
# # hist = np.bincount(gray_img.ravel(),minlength=256); plt.plot(hist)
# # hist, bin = np.histogram(gray_img.ravel(), 256, [0, 256]); plt.plot(hist)
# plt.hist(gray_img.ravel(), 256, [1, 256])
# plt.show()



"""plot histogram using mask """
# # create a mask
# mask = np.zeros(img.shape[:2], np.uint8)
# mask[100:430, 50:550] = 255
# masked_img = cv2.bitwise_and(img,img,mask = mask)

# # Calculate histogram with mask and without mask
# # Check third argument for mask
# hist_full = cv2.calcHist([img],[0],None,[256],[1,256])
# hist_mask = cv2.calcHist([img],[0],mask,[256],[1,256])

# plt.subplot(221), plt.imshow(img, 'gray')
# plt.subplot(222), plt.imshow(mask,'gray')
# plt.subplot(223), plt.imshow(masked_img, 'gray')
# plt.subplot(224), plt.plot(hist_full), plt.plot(hist_mask)
# plt.xlim([0,256])
# plt.show()

# print('masked image', masked_img.shape)
# # cv2.imwrite('onigiri_masked.jpg',masked_img)




""" split and plot RGB  """

""" RGB color """ #true RGB colors
# im = onigiriC
# im_R = im.copy()
# im_R[:, :, (1, 2)] = 0
# im_G = im.copy()
# im_G[:, :, (0, 2)] = 0
# im_B = im.copy()
# im_B[:, :, (0, 1)] = 0

# im_RGB = np.concatenate((im_R, im_G, im_B), axis=1)
# # im_RGB = np.hstack((im_R, im_G, im_B))
# # im_RGB = np.c_['1', im_R, im_G, im_B]

# pil_img = Image.fromarray(im_RGB)
# plt.imshow(im_RGB)
""" RGB color """


""" read images in different format """
# img2 = cv2.cvtColor(img, cv2.IMREAD_COLOR)
# img2 = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
# img2 = cv2.cvtColor(img, cv2.COLOR_RGB2HSV)


""" plot using matplotlib """
# plt1 = plt.figure(1)
# plt1.suptitle('main image')
# plt.imshow(img2)
 
# plt2 = plt.figure(2)
# plt2.suptitle('rgb split')
# plt.subplot(131)
# plt.imshow(img2[:,:,0])
# plt.subplot(132)
# plt.imshow(img2[:,:,1])
# plt.subplot(133)
# plt.imshow(img2[:,:,2])

# plt.show(block=False)
# plt.pause(10)


""" split b, g, r """
# b, g, r = cv2.split(img2) #opencv reads the images as BGR instead of RGB
# plt3 = plt.figure(3)
# plt3.suptitle('using split into b g r')
# plt.subplot(131)
# plt.imshow(b)
# plt.subplot(132)
# plt.imshow(g)
# plt.subplot(133)
# plt.imshow(r)

# plt.show(block=False)
# plt.pause(10)
# plt.close(plt1) 
# plt.close(plt2)
# plt.close(plt3)






# colorsegmentation
""" generating the colored scatter plot for the image in HSV """
""" https://realpython.com/python-opencv-color-spaces/ """



from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib import colors

hsv_img = cv2.cvtColor(img, cv2.COLOR_RGB2HSV)
h, s, v = cv2.split(hsv_img)
fig = plt.figure()
axis = fig.add_subplot(1, 1, 1, projection="3d")


pixel_colors = img.reshape((np.shape(img)[0]*np.shape(img)[1], 3))
norm = colors.Normalize(vmin=-1.,vmax=1.)
norm.autoscale(pixel_colors)
pixel_colors = norm(pixel_colors).tolist()

axis.scatter(h.flatten(), s.flatten(), v.flatten(), facecolors=pixel_colors, marker=".")
axis.set_xlabel("Hue")
axis.set_ylabel("Saturation")
axis.set_zlabel("Value")
plt.show()








""" https://docs.opencv.org/3.4/d8/dbc/tutorial_histogram_calculation.html """
# plot histogarm  using cv2(40x faster)


"""
What does this program do?
->Loads an image
->Splits the image into its R, G and B planes using the function cv::split
->Calculate the Histogram of each 1-channel plane by calling the function cv::calcHist
->Plot the three histograms in a window 
"""


# cv = cv2

# bgr_planes = cv.split(img)

# histSize = 256

# histRange = (0, 256) # the upper boundary is exclusive

# accumulate = False

# b_hist = cv.calcHist(bgr_planes, [0], None, [histSize], histRange, accumulate=accumulate)
# g_hist = cv.calcHist(bgr_planes, [1], None, [histSize], histRange, accumulate=accumulate)
# r_hist = cv.calcHist(bgr_planes, [2], None, [histSize], histRange, accumulate=accumulate)

# hist_w = 480
# hist_h = 640

# bin_w = int(round( hist_w/histSize ))

# histImage = np.zeros((hist_h, hist_w, 3), dtype=np.uint8)

# cv.normalize(b_hist, b_hist, alpha=0, beta=hist_h, norm_type=cv.NORM_MINMAX)
# cv.normalize(g_hist, g_hist, alpha=0, beta=hist_h, norm_type=cv.NORM_MINMAX)
# cv.normalize(r_hist, r_hist, alpha=0, beta=hist_h, norm_type=cv.NORM_MINMAX)

# for i in range(1, histSize):
#     cv.line(histImage, ( bin_w*(i-1), hist_h - int(np.round(b_hist[i-1])) ),
#             ( bin_w*(i), hist_h - int(np.round(b_hist[i])) ),
#             ( 255, 0, 0), thickness=2)
#     cv.line(histImage, ( bin_w*(i-1), hist_h - int(np.round(g_hist[i-1])) ),
#             ( bin_w*(i), hist_h - int(np.round(g_hist[i])) ),
#             ( 0, 255, 0), thickness=2)
#     cv.line(histImage, ( bin_w*(i-1), hist_h - int(np.round(r_hist[i-1])) ),
#             ( bin_w*(i), hist_h - int(np.round(r_hist[i])) ),
#             ( 0, 0, 255), thickness=2)
    
    
# cv.imshow('Source image', img)
# cv.imshow('calcHist Demo', histImage)
# cv2.waitKey(5000); cv2.destroyAllWindows()