import argparse

import cv2
import time
import numpy as np

weights = 'onigiriTx/' + 'yolov3-onigiri.backup'
config = 'onigiriTx/' + 'yolov3-onigiri.cfg'
names = 'onigiriTx/' + 'onigiri.names'
image = 'onigiriTx/' + 'images/25.png'

""" test images """
# python3 testOnigiri.py  --image  test/000256.png 
# ./darknet detector train onigiriYolo2/detector.data onigiriYolo2/cfg/yolov3-onigiri.cfg onigiriYolo2/backup/yolov3-onigiri.backup
# python3 testOnigiri.py --image test/000223.png  --config cfg/yolov3-onigiri.cfg --weights ../backup/yolov3-onigiri_900.weights  --names onigiri.names



CONF_THRESH, NMS_THRESH = 0.6, 0.5

# Load the network
net = cv2.dnn.readNetFromDarknet(config, weights)
net.setPreferableBackend(cv2.dnn.DNN_BACKEND_OPENCV)
net.setPreferableTarget(cv2.dnn.DNN_TARGET_CPU)

# Read and convert the image to blob and perform forward pass to get the bounding boxes with their confidence scores
img = cv2.imread(image)
height, width = img.shape[:2]

# Get the output layer from YOLO
layers = net.getLayerNames()
output_layers = [layers[i[0] - 1] for i in net.getUnconnectedOutLayers()]

blob = cv2.dnn.blobFromImage(img, 0.00392, (416, 416), swapRB=True, crop=False)
net.setInput(blob)
start = time.time()
layer_outputs = net.forward(output_layers)
end = time.time()



# show timing information on YOLO
print("[INFO] YOLO took {:.6f} seconds".format(end - start))

class_ids, confidences, boxes = [], [], []

for output in layer_outputs:

    for detection in output:

        scores = detection[5:]
        class_id = np.argmax(scores)
        confidence = scores[class_id]

        if confidence > CONF_THRESH:

            center_x, center_y, w, h = (detection[0:4] * np.array([width, height, width, height])).astype('int')

            x = int(center_x - w / 2)
            y = int(center_y - h / 2)

            boxes.append([x, y, int(w), int(h)])
            confidences.append(float(confidence))
            class_ids.append(int(class_id))

            
# print("confidence", confidences)
# print("boxes", boxes)

# Draw the filtered bounding boxes with their class to the image
with open(names, "r") as f:
    classes = [line.strip() for line in f.readlines()]
# colors = np.random.uniform(0, 255, size=(len(classes), 3))
color = (255,0,255)

# Perform non maximum suppression for the bounding boxes to filter overlapping and low confident bounding boxes
indices = cv2.dnn.NMSBoxes(boxes, confidences, CONF_THRESH, NMS_THRESH)
# print("indices", indices)

if len(indices)>0:
    for idx in indices.flatten():
        (x, y) = (boxes[idx][0], boxes[idx][1])
        (w, h) = (boxes[idx][2], boxes[idx][3])

        # cv2.rectangle(img, (x, y), (x + w, y + h), colors[idx], 2)
        cv2.rectangle(img, (x, y), (x + w, y + h), color, 2)
        cv2.putText(img, classes[class_ids[idx]], (x, y -5), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, color, 2)
        
cv2.imshow("image", img)
cv2.waitKey(0)


