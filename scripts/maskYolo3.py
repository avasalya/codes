#!/usr/bin/env python3
# -*- coding: utf-8 -*-

""" real-time yolov3-masking with realsense D435
    16/June/2020, Ashesh Vasalya, AIST
"""

""" train yolo """
# ./darknet detector train onigiriYolo2/detector.data onigiriYolo2/cfg/yolov3-onigiri.cfg onigiriYolo2/backup/yolov3-onigiri.backup

""" validate yolo """
# ./darknet detector test onigiriYolo2/detector.data onigiriYolo2/cfg/yolov3-onigiri.cfg onigiriYolo2/backup/yolov3-onigiri_11000.weights  < onigiriYolo2/train.txt > onigiriYolo2/results/result.txt

# import modules
import os
import time
import getpass
import argparse
import cv2 as cv
import numpy as np
import random as rng
import pyrealsense2 as rs



class maskYolo():
    
    def __init__(self, confThresh, nmsThresh, size):

        self.CONF_THRESH = confThresh
        self.NMS_THRESH = nmsThresh
        self.size = size
        
        # Wait for frame (Color & Depth)
        frames = pipeline.wait_for_frames()
        color_frame = frames.get_color_frame()
        depth_frame = rs.align(rs.stream.color).process(frames).get_depth_frame()
        
        if  not depth_frame or  not color_frame:
            raise ValueError('No image found, camera not streaming?')

        # color image
        self.rgb = np.asanyarray(color_frame.get_data())
        
        # Depth image
        depth_color_frame = rs.colorizer().colorize(depth_frame)
        self.depth = np.asanyarray(depth_color_frame.get_data())


    def model(self, net):

        self.height, self.width = self.rgb.shape[:2]

        # Get the output layer from YOLO
        layers = net.getLayerNames()
        output_layers = [layers[i[0] - 1] for i in net.getUnconnectedOutLayers()]

        # convert the image to blob
        blob = cv.dnn.blobFromImage(self.rgb, 0.00392, (self.size, self.size), swapRB=True, crop=False)
        net.setInput(blob)
        
        # perform forward pass to get the bounding boxes with their confidence scores
        start = time.time()
        self.layer_outputs = net.forward(output_layers)        
        end = time.time()

        # show timing information on YOLO
        print("[INFO] YOLO took {:.6f} seconds".format(end - start))


    def detection(self):
        
        self.boxes, self.confidences, self.class_ids = [], [], []

        for output in self.layer_outputs:

            for detection in output:
                scores = detection[5:]
                class_id = np.argmax(scores)
                confidence = scores[class_id]

                if confidence > self.CONF_THRESH:

                    center_x, center_y, w, h = (detection[0:4] * np.array([self.width, self.height, self.width, self.height])).astype('int')
                    
                    x = int(center_x - w / 2) - 10
                    y = int(center_y - h / 2) - 10

                    self.boxes.append([x, y, int(w+10), int(h+10)])
                    self.confidences.append(float(confidence))
                    self.class_ids.append(int(class_id))

        # print("confidence", self.confidences)
        # print("total boxes", len(self.boxes))


    def nms(self, box, confThresh=None, overlapThresh=0.3):
        
        """ https://www.pyimagesearch.com/2015/02/16/faster-non-maximum-suppression-python/ """

        # if there are no boxes, return an empty list
        if len(box) == 0:
            print("no box found")
            return []

        # # if the bounding boxes integers, convert them to floats --
        # # this is important since we'll be doing a bunch of divisions
        boxes = np.array(box)

        # initialize the list of picked indexes	
        pick = []

        # grab the coordinates of the bounding boxes
        x1 = boxes[:,0]
        y1 = boxes[:,1]
        x2 = boxes[:,2]
        y2 = boxes[:,3]

        # compute the area of the bounding boxes and sort the bounding
        # boxes by the bottom-right y-coordinate of the bounding box
        area = (x2 - x1 + 1) * (y2 - y1 + 1)
        idxs = y2
        
        # if confidence probabilities are provided, sort on them instead
        if confThresh is not None:
            idxs = confThresh

        # sort the indexes
        idxs = np.argsort(idxs)
        
        # keep looping while some indexes still remain in the indexes list
        while len(idxs) > 0:

            # grab the last index in the indexes list and add the
            # index value to the list of picked indexes
            last = len(idxs) - 1
            i = idxs[last]
            pick.append(i)

            # find the largest (x, y) coordinates for the start of
            # the bounding box and the smallest (x, y) coordinates
            # for the end of the bounding box
            xx1 = np.maximum(x1[i], x1[idxs[:last]])
            yy1 = np.maximum(y1[i], y1[idxs[:last]])
            xx2 = np.minimum(x2[i], x2[idxs[:last]])
            yy2 = np.minimum(y2[i], y2[idxs[:last]])

            # compute the width and height of the bounding box
            w = np.maximum(0, xx2 - xx1 + 1)
            h = np.maximum(0, yy2 - yy1 + 1)

            # compute the ratio of overlap
            overlap = (w * h) / area[idxs[:last]]
                    
            # delete all indexes from the index list that have
            idxs = np.delete(idxs, np.concatenate(([last], np.where(overlap > overlapThresh)[0])))
            # return only the bounding boxes that were picked using the
            # integer data type
        return boxes[pick].astype("int")


    def drawBoxes(self):
                 
        # Draw the filtered bounding boxes with their class to the image
        with open(args.names, "r") as f:
            classes = [line.strip() for line in f.readlines()]
        color = (255,0,255)

        # Perform non maximum suppression for the bounding boxes to filter overlapping and low confident bbox
        pick = self.nms(self.boxes, self.confidences, self.NMS_THRESH)
        # print("after applying non-maximum, total bounding boxes", len(boxes), "reduced to", (len(pick)))
        
        # draw bounding self.boxes
        i = 0
        self.x_, self.y_, self.w_, self.h_ = [], [], [], []
        
        for (x, y, w, h) in pick:
            
            if drawBbox:
                cv.rectangle(self.rgb, (x, y), (x + w, y + h), color, 2)
                cv.rectangle(self.depth, (x, y), (x + w, y + h), color, 2)
            else:
                pass
            
            if i < len(pick):
                if drawBbox:
                    cv.putText(self.rgb, classes[self.class_ids[i]], (x, y-5), cv.FONT_HERSHEY_PLAIN, 1, color, 1)
                    cv.putText(self.depth, classes[self.class_ids[i]], (x, y-5), cv.FONT_HERSHEY_PLAIN, 1, color, 1)
                else:
                    pass
                    
                self.x_.append(x)
                self.y_.append(y)
                self.w_.append(w)
                self.h_.append(h)

                i+=1


    def maskImages(self):
      
        self.roi = np.zeros(self.rgb.shape, dtype='uint8')
        self.roid = np.zeros(self.depth.shape, dtype='uint8')
  
        for i in range(len(self.x_)):
            
            # create mask
            self.roi[self.y_[i] : self.y_[i] + self.h_[i], self.x_[i] : (self.x_[i] + self.w_[i])] = 255
            self.roid[self.y_[i] : self.y_[i] + self.h_[i], self.x_[i] : (self.x_[i] + self.w_[i])] = 255
        
        self.maskrgb = cv.bitwise_and(self.rgb, self.roi)
        self.maskdepth = cv.bitwise_and(self.depth, self.roid)
     
     
    def blurImages(self):
 
        self.blur = cv.GaussianBlur(self.rgb, (41, 41), 0)
        self.blur = np.where(self.roi==np.array([255, 255, 255]), self.rgb, self.blur)


    def visualize(self, *args):
   
        self.images = np.hstack(*args)
        cv.imshow( 'onigiri', self.images)
  

if __name__ == '__main__':

    # clean terminal in the beginning
    username = getpass.getuser()
    osName = os.name
    if osName == 'posix':
        os.system('clear')
    else:
        os.system('cls')
    
    
    # onigiri yolo parameters
    parser = argparse.ArgumentParser(add_help=False)
    parser.add_argument("--names", default='yolov3/onigiri.names', help="class names path")
    parser.add_argument("--weights", default='yolov3/yolo-obj_final.weights', help="YOLO weights path")
    parser.add_argument("--config", default='yolov3/yolo-obj.cfg', help="YOLO config path")
    args = parser.parse_args()

    # load network
    net = cv.dnn.readNetFromDarknet(args.config, args.weights)
    net.setPreferableBackend(cv.dnn.DNN_BACKEND_CUDA)
    net.setPreferableTarget(cv.dnn.DNN_TARGET_CUDA)


    # Stream (Color/Depth) settings
    config = rs.config()
    config.enable_stream(rs.stream.color, 640 , 480 , rs.format.bgr8, 60)
    config.enable_stream(rs.stream.depth, 640 , 480 , rs.format.z16, 60)
    config.enable_stream(rs.stream.infrared, 640 , 480 , rs.format.y8, 60)


    # Start streaming
    pipeline = rs.pipeline()
    profile = pipeline.start(config)

    drawBbox = False
    itr = 1

    try:
        while True:
            
            yolo = maskYolo(0.4, 0.9, 416)
            
            yolo.model(net)
            yolo.detection()
            yolo.drawBoxes()
            yolo.maskImages()
            yolo.blurImages()
            
            yolo.visualize((yolo.rgb, yolo.maskrgb, yolo.blur, yolo.depth, yolo.maskdepth))

            key = cv.waitKey(1) & 0xFF
            if  key == 27:
                print("stopping streaming...")
                break
            elif key == ord("b"):
                itr+=1
                if itr % 2 == 0:
                    drawBbox = True
                else:
                    drawBbox = False           
            
    finally:
        pipeline.stop()
        cv.destroyAllWindows()