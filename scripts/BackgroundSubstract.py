import os
import sys
import pickle
import getpass
import cv2 as cv
import numpy as np
import open3d.open3d as o3d
import matplotlib.pyplot as plt

username = getpass.getuser()
osName = os.name
if osName == 'posix':
    os.system('clear')
else:
    os.system('cls')

path = os.path.dirname(__file__) + '/onigiriGrid/w-wo-banju/'
# print(path)


# color & depth background substraction
""" http://grauonline.de/wordpress/?page_id=3065 """

""" TO DO """
# extract contours based on distance (distance transform)
# filter noise and image sharpning (cv.filter2d)
# dialation 
# edge detection
# normalize brightness (folder b & d)
""" TO DO """

# use this to extract individual onigiris 
""" https://docs.opencv.org/3.4/d2/dbd/tutorial_distance_transform.html """


# banjud = pickle.load(open(path + 'd/0/depth.pkl', 'rb'), encoding='bytes')
# onigirid = pickle.load(open(path + 'd/1/depth.pkl', 'rb'), encoding='bytes')

# plt.imshow(banju)
# plt.show(block=False)
# plt.pause(5)


V1 = True
V2 = False
V3 = False



""" Version 1.0 """
if V1 is True:
    
    print('running V1 version')
    
    # reading images
    banjud = cv.imread(path + 'a/0/depth1.png', cv.CV_16UC1)
    banjud = np.uint8(banjud) # need better way to convert
    banjud_copy = banjud.copy()
    print(banjud_copy.shape)

    onigiriFolder = 'a/3/'

    onigiriGray = cv.imread(path + onigiriFolder + 'image.png', 0)
    onigirid = cv.imread(path + onigiriFolder + 'depth1.png', cv.CV_16UC1)
    onigirid = np.uint8(onigirid)
    onigirid_copy = onigirid.copy()
    print(onigirid_copy.shape)

    # taking abs difference in pixel depth
    depthDiff = cv.subtract(banjud_copy, onigirid_copy)

    mean = np.mean(depthDiff[depthDiff > 0]) # alternative mean/median
    std = np.std(depthDiff[depthDiff > 0])
    print('mean', mean)
    print('std', std)

    ret, thresh = cv.threshold(depthDiff, abs(mean-std) , 255, cv.THRESH_BINARY)
    thresh = cv.morphologyEx(thresh, cv.MORPH_DILATE, cv.getStructuringElement(cv.MORPH_ELLIPSE, (1,1))); #a,c


    """ https://stackoverflow.com/questions/60285314/remove-undesired-connected-pixels-from-an-image-with-python """
    def badContour(c): # Decide what I want to find and its features
        peri = cv.contourArea(c) # Find areas
        good =  peri > 500
        return  good # Large area is considered "good"

    # Find and Draw contours
    # thresh_8u = thresh.astype('uint8')
    contours, hierarchy = cv.findContours(thresh, cv.RETR_CCOMP, cv.CHAIN_APPROX_SIMPLE)
    print('total contours', len(contours))
    drawing = np.zeros((thresh.shape[0], thresh.shape[1], 3))
    for c in contours:
        if badContour(c):
            cv.drawContours(drawing, [c], -1,  (0,255,0),  1, cv.LINE_4, hierarchy, 0)

    # draw largest contours
    c = max(contours, key = cv.contourArea)
    x,y,w,h = cv.boundingRect(c)
    # draw the biggest contour (c) in green
    rectimg = cv.rectangle(drawing,(x,y),(x+w,y+h),(255,0,0),2)
    cv.rectangle(onigiriGray,(x,y),(x+w,y+h),(255,0,0),2)


    # figures
    imgStack1 = np.hstack((banjud_copy, onigirid_copy, onigiriGray, depthDiff, thresh))
    cv.imshow('banjud_copy, onigirid_copy, gray, depthDiff, thresh', imgStack1)
    cv.imshow('contours', drawing)

    cv.waitKey()



""" Version 2.0 """
if V2 is True:

    print('running V2 version')

    # reading images
    banjud = cv.imread(path + 'd/0/depth1.png', cv.CV_16UC1)

    banjud = np.uint8(banjud) # need better way to convert
    banjud_copy = banjud.copy()
    print(banjud_copy.shape)

    onigiriFolder = 'd/2/'

    onigiriGray = cv.imread(path + onigiriFolder + 'image.png', 0)
    onigirid = cv.imread(path + onigiriFolder + 'depth1.png', cv.CV_16UC1)

    onigirid = np.uint8(onigirid)
    onigirid_copy = onigirid.copy()
    print(onigirid_copy.shape)


    # taking abs difference in pixel depth
    depthDiff = cv.subtract(banjud_copy, onigirid_copy)


    # onigirid_copy = cv.equalizeHist(onigiriGray)
    # res = np.hstack((onigiriGray,equ)) #stacking images side-by-side
    # cv.imshow('res.png',res)

    # depthDiff = cv.normalize(depthDiff, depthDiff, 0, 255, cv.NORM_MINMAX)
    # cv.imshow('dst_rt', depthDiff)


    # thresholding using mean/std
    # mean, STD  = cv2.meanStdDev(ROI)
    mean = np.mean(depthDiff[depthDiff > 0]) # alternative mean/median
    std = np.std(depthDiff[depthDiff > 0])
    print('mean', mean)
    print('std', std)

    ret, th = cv.threshold(depthDiff, abs(mean-std) , 255, cv.THRESH_BINARY)
    # thresh = cv.adaptiveThreshold(depthDiff, 255, cv.ADAPTIVE_THRESH_GAUSSIAN_C, cv.THRESH_BINARY_INV, 11, 2)

    morph = cv.morphologyEx(th, cv.MORPH_DILATE, cv.getStructuringElement(cv.MORPH_ELLIPSE, (1,1))); #a,c
    # thresh = cv.morphologyEx(thresh, cv.MORPH_DILATE, cv.getStructuringElement(cv.MORPH_ELLIPSE, (10,10))); #d

    # thresh = morph.copy()
    thresh = cv.dilate(morph, cv.getStructuringElement(cv.MORPH_ELLIPSE, (10,10)))


    """ https://stackoverflow.com/questions/60285314/remove-undesired-connected-pixels-from-an-image-with-python """
    def badContour(c): # Decide what I want to find and its features
        peri = cv.contourArea(c) # Find areas
        good =  peri > 500
        return  good # Large area is considered "good"


    # Find and Draw contours
    contours, hierarchy = cv.findContours(thresh, cv.RETR_CCOMP, cv.CHAIN_APPROX_SIMPLE)
    print('total contours', len(contours))
    drawing = np.zeros((thresh.shape[0], thresh.shape[1], 3))
    for c in contours:
        if badContour(c):
            cv.drawContours(drawing, [c], -1,  (0,255,0),  1, cv.LINE_4, hierarchy, 0)

    # draw largest contours
    c = max(contours, key = cv.contourArea)
    x,y,w,h = cv.boundingRect(c)
    # draw the biggest contour (c) in green
    rectimg = cv.rectangle(drawing,(x,y),(x+w,y+h),(255,0,0),2)
    cv.rectangle(onigiriGray,(x,y),(x+w,y+h),(255,0,0),2)


    # figures
    imgStack1 = np.hstack((banjud_copy, onigirid_copy, onigiriGray, depthDiff, thresh))
    cv.imshow('banjud_copy, onigirid_copy, gray, depthDiff, thresh', imgStack1)
    cv.imshow('contours', drawing)

    # this = cv.bitwise_not(thresh, onigiriGray)
    # cv.imshow('and', this)

    cv.waitKey()




""" Version 3.0 """
if V3 is True:
    
    print('running V3 version')
    
    # reading images
    banjud = cv.imread(path + 'a/0/depth1.png', cv.CV_16UC1)
    banjud = np.uint8(banjud) # need better way to convert
    banjud_copy = banjud.copy()
    print(banjud_copy.shape)

    onigiriFolder = 'a/1/'

    onigiriC = cv.imread(path + onigiriFolder + 'image.png')
    onigiriGray = cv.imread(path + onigiriFolder + 'image.png', 0)
    onigirid = cv.imread(path + onigiriFolder + 'depth1.png', cv.CV_16UC1)
    onigirid = np.uint8(onigirid)
    onigirid_copy = onigirid.copy()
    print(onigirid_copy.shape)

    # taking abs difference in pixel depth
    depthDiff = cv.subtract(banjud_copy, onigirid_copy)

    mean = np.mean(depthDiff[depthDiff > 0]) # alternative mean/median
    std = np.std(depthDiff[depthDiff > 0])
    print('mean', mean)
    print('std', std)

    ret, thresh = cv.threshold(depthDiff, abs(mean-std) , 255, cv.THRESH_BINARY)
    thresh = cv.morphologyEx(thresh, cv.MORPH_DILATE, cv.getStructuringElement(cv.MORPH_ELLIPSE, (1,1))); #a,c



    from skimage.feature import peak_local_max
    from skimage.morphology import watershed
    from scipy import ndimage

    thresh2 = cv.threshold(onigiriGray, 0, 255, cv.THRESH_BINARY | cv.THRESH_OTSU)[1]

    # compute the exact Euclidean distance from every binary
    # pixel to the nearest zero pixel, then find peaks in this
    # distance map
    D = ndimage.distance_transform_edt(thresh2)
    localMax = peak_local_max(D, indices=False, min_distance=20, labels=thresh2)
    # perform a connected component analysis on the local peaks,
    # using 8-connectivity, then appy the Watershed algorithm
    markers = ndimage.label(localMax, structure=np.ones((3, 3)))[0]
    labels = watershed(-D, markers, mask=thresh2)
    print("[INFO] {} unique segments found".format(len(np.unique(labels)) - 1))
    # cv.imshow('distance',D)


    """ create back mask """
    # mask = np.zeros(onigiriGray.shape, dtype="uint8")
    # mask[mask] = 0
    # cv.imshow('mask', mask)


    """ https://stackoverflow.com/questions/60285314/remove-undesired-connected-pixels-from-an-image-with-python """
    def desiredContours(c): # Decide what I want to find and its features
        peri = cv.contourArea(c) # Find areas
        good =  peri > 500
        # good =  (peri < 2000 and peri > 500)
        return  good # Large area is considered "good"

    # Find and Draw contours
    contours, hierarchy = cv.findContours(thresh, cv.RETR_CCOMP, cv.CHAIN_APPROX_SIMPLE)
    print('total contours', len(contours))
    
    drawing = np.zeros((thresh.shape[0], thresh.shape[1], 3))
    
    for c in contours:
        
        # if len(c) > 5:
        #     ellipse = cv.fitEllipse(c)
        #     cv.ellipse(onigiriC, ellipse, (0,255,0), 2)
        
        if desiredContours(c):
            # cv.drawContours(drawing, [c], -1,  (0,255,0),  1, cv.LINE_4, hierarchy, 0)
            cv.drawContours(drawing, [c], 0,  (0,255,0),  1, cv.LINE_4, hierarchy, 0)


    thresh = cv.bitwise_and(onigiriGray.copy(), thresh.copy())
    
                        
    # draw largest contours
    c = max(contours, key = cv.contourArea)
    x,y,w,h = cv.boundingRect(c)
    # draw the biggest contour (c) in green
    rectimg = cv.rectangle(drawing,(x,y),(x+w,y+h),(255,0,0),2)
    cv.rectangle(onigiriC,(x,y),(x+w,y+h),(0,255,0),2)
    # cv.imshow('onigiri', onigiriC)

    
    
    # figures
    imgStack1 = np.hstack((banjud_copy, onigirid_copy, onigiriGray, depthDiff, thresh))
    cv.imshow('banjud_copy, onigirid_copy, gray, depthDiff, thresh', imgStack1)

    # imgStack1 = np.hstack((banjud_copy, onigirid_copy, thresh))
    # cv.imshow('banjud_copy, onigirid_copy thresh', imgStack1)
  

    cv.imshow('contours', drawing)
    # cv.imshow('roi', onigiriC)
    
    
    cv.waitKey()


































""" attempts """

# color_raw = o3d.io.read_image(path + "a/1/image.png")
# depth_raw = o3d.io.read_image(path + "a/1/depth.png")
# rgbd_image = o3d.geometry.RGBDImage.create_from_color_and_depth(color_raw, depth_raw)

# # though this default parameters are also doing ok ok job
# pcd = o3d.geometry.PointCloud.create_from_rgbd_image(
#     rgbd_image,
#     o3d.camera.PinholeCameraIntrinsic(
#         o3d.camera.PinholeCameraIntrinsicParameters.PrimeSenseDefault))
# # Flip it, otherwise the pointcloud will be upside down
# pcd.transform([[1, 0, 0, 0], [0, -1, 0, 0], [0, 0, -1, 0], [0, 0, 0, 1]])
# o3d.visualization.draw_geometries([pcd])



# mask = cv.bitwise_or(depthDiff, thresh)

# mask = thresh.copy()
# mask[mask > mean*0.1 ] = 0
# mask[mask < mean*0.1 ] = 0
# mask[mask > 0] = 1


# contours, hierarchy  = cv.findContours(thresh, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE) 
# # contours, hierarchy = cv.findContours(thresh, 2, 1)
# contours = sorted(contours, key=cv.contourArea)
# drawing = np.zeros_like(thresh)
# # drawing = np.zeros((thresh.shape[0], thresh.shape[1], 3))


# cv.drawContours(drawing, contours, -1, (0,255,0), 2, cv.LINE_8, hierarchy, 0)
# cv.drawContours(drawing, [contours[-1]], -1, 255, cv.FILLED, 1)
# cv.drawContours(out_mask, contours, -1, 255, cv.FILLED, 1)

# # To draw all the contours in an image:
# cv.drawContours(img, contours, -1, (0,255,0), 3)
# To draw an individual contour, say 4th contour:
# cv.drawContours(img, contours, 3, (0,255,0), 3)
# But most of the time, below method will be useful:
# cnt = contours[4]
# cv.drawContours(img, [cnt], 0, (0,255,0), 3)

# out = onigirid.copy()
# out[out_mask == 0] = 0



# imgStack2 = np.hstack((depthDiff, thresh))
# cv.imshow('depthDiff, thresh', imgStack2)





# color background substraction from video
""" https://docs.opencv.org/3.4/d1/dc5/tutorial_background_subtraction.html """
""" https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_video/py_bg_subtraction/py_bg_subtraction.html """

